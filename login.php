<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $ses_id = session_id();
    if (empty($ses_id)) {
        session_start();
        $ses_id = session_id();
    }
    if (isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['pass']) && !empty($_POST['pass'])) {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && strlen($_POST['pass']) > 5) {
            include "controller.php";
            $check = $control->check_user($_POST['email'], $_POST['pass']);
            if (!empty($check)) {
                $_SESSION['email'] = $_POST['email'];
                $_SESSION['id'] = $check['id'];
                $control->update_user_info($_SESSION['email'], ['online' => $ses_id]);
                header("location:profile.php");
            } else {
                $_SESSION['error'] = "Incorrect email or password";
                header("location:index.php");
            }
        } else {
            $_SESSION['error'] = "Please input correct email and password";
            header("location:index.php");
        }
    } else {
        $_SESSION['error'] = "Please fill all fields";
        header("location:index.php");
    }
} else {
    header("location:index.php");
}