<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("location:index.php");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include "controller.php";
    if (isset($_POST['old']) && !empty($_POST['old'])) {
        if ($control->check_user($_SESSION['email'], $_POST['old'])) {
            $update = [];
            if (isset($_POST['name']) && !empty($_POST['name'])) {
                $update['name'] = $_POST['name'];
            }
            if (isset($_POST['surname']) && !empty($_POST['surname'])) {
                $update['surname'] = $_POST['surname'];
            }
            if (isset($_POST['dob']) && !empty($_POST['dob']) && strtotime($_POST['dob'])) {
                $update['date_of_birth'] = $_POST['dob'];
            }
            if (isset($_POST['pass']) && !empty($_POST['pass']) && isset($_POST['confirm']) && !empty($_POST['confirm']) && $_POST['pass'] === $_POST['confirm'] && strlen($_POST['pass']) > 5) {
                $update['password'] = $_POST['pass'];
            }
            if (isset($_POST['work_place']) && !empty($_POST['work_place'])) {
                $update['work_place'] = $_POST['work_place'];
            }
            if (isset($_POST['work_as']) && !empty($_POST['work_as'])) {
                $update['work_as'] = $_POST['work_as'];
            }
            if (isset($_POST['marital_status']) && !empty($_POST['marital_status'])) {
                $update['marital_status'] = $_POST['marital_status'];
            }
            if (isset($_POST['city']) && !empty($_POST['city'])) {
                $update['city'] = $_POST['city'];
            }
            if (isset($_POST['country']) && !empty($_POST['country'])) {
                $update['country'] = $_POST['country'];
            }
            $control->update_user_info($_SESSION['email'], $update);
        } else {
            $_SESSION['error'] = "Wrong password";
        }
    } elseif (isset($_FILES) && !empty($_FILES)) {
        $user = $control->get_user_info($_SESSION['id']);
        if (!empty($_FILES['avatar']) && !$_FILES['avatar']['error']) {
            if (!($user['avatar'] == 'images/default_man.png' || $user['avatar'] == 'images/default_girl.png')) {
                unlink($user['avatar']);
            }
            if (!file_exists('images/users_photos/' . $_SESSION['id'])) {
                mkdir('images/users_photos/' . $_SESSION['id']); //create images folder, if it not exist
            }
            $upload_dir = 'images/users_photos/' . $_SESSION['id'] . '/' . uniqid() . $_FILES['avatar']['name'];
            move_uploaded_file($_FILES['avatar']['tmp_name'], $upload_dir);
            $control->update_user_info($_SESSION['email'], ['avatar' => $upload_dir]);
        } elseif (!empty($_FILES['wallpaper']) && !$_FILES['wallpaper']['error']) {
            if ($user['wallpaper'] != 'images/wall.png') {
                unlink($user['wallpaper']);
            }
            if (!file_exists('images/users_photos/' . $_SESSION['id'])) {
                mkdir('images/users_photos/' . $_SESSION['id']); //create images folder, if it not exist
            }
            $upload_dir = 'images/users_photos/' . $_SESSION['id'] . '/' . uniqid() . $_FILES['wallpaper']['name'];
            move_uploaded_file($_FILES['wallpaper']['tmp_name'], $upload_dir);
            $control->update_user_info($_SESSION['email'], ['wallpaper' => $upload_dir]);
        } elseif (!empty($_FILES['new_photo']) && !$_FILES['new_photo']['error']) {
            if (!file_exists('images/users_photos/' . $_SESSION['id'])) {
                mkdir('images/users_photos/' . $_SESSION['id']); //create images folder, if it not exist
            }
            $upload_dir = 'images/users_photos/' . $_SESSION['id'] . '/' . uniqid() . $_FILES['new_photo']['name'];
            move_uploaded_file($_FILES['new_photo']['tmp_name'], $upload_dir);
            $control->add_photo($_SESSION['id'], $upload_dir, $_POST['description'], time());
        } else {
            $_SESSION['error'] = "Photo not uploaded. May be it's size is bigger than 2MB";
        }
    }
    header("location:profile.php");
} else {
    header("location:profile.php");
}