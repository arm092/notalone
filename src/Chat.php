<?php
namespace ChatApp;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class Chat implements MessageComponentInterface
{
    protected $clients;
    protected $db;
    protected $users;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        //$this->db = new PDO('mysql:host=localhost;dbname=notalone;charset=utf8', 'root', ''); #\MySQLi("localhost", "root", "", "notalone");
        //$this->db->set_charset("utf8");
        $this->users = [];
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        //$sessionId = $conn->WebSocket->request->getCookies()['PHPSESSID'];
        $this->users[$conn->resourceId] = $conn->resourceId;
        echo "New connection! connected... ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        // $numRecv = count($this->clients) - 1;
        // echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
        //     , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');
        $message = json_decode($msg, true);
        // $text = $message['message'];
        // $from_id = $message['from_id'];
        // $to = $message['to_id'];
        // $date = time();

        //$this->db->query("INSERT INTO messages (message, from_id, to_id, date) VALUES ('$text', '$from_id', '$to', '$date')");

        foreach ($this->clients as $client) {
            //if ($message['session'] == $_COOKIE['PHPSESSID'] || $client->resourceId == $from->resourceId) {
            // The sender is not the receiver, send to each client connected
            $client->send($msg);
            //}
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}