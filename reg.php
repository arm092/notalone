<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $ses_id = session_id();
    if (empty($ses_id)) {
        session_start();
        $ses_id = session_id();
    }
    if (isset($_POST['name']) && !empty($_POST['name']) && isset($_POST['surname']) && !empty($_POST['surname']) && isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['pass']) && !empty($_POST['pass']) && isset($_POST['confirm']) && !empty($_POST['confirm']) && isset($_POST['sex']) && !empty($_POST['sex']) && isset($_POST['dob']) && !empty($_POST['dob'])) {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && $_POST['pass'] === $_POST['confirm'] && strlen($_POST['pass']) > 5 && strtotime($_POST['dob'])) {
            include "controller.php";
            $emails_list = $control->get_emails();
            if (in_array(['email' => $_POST['email']], $emails_list)) {
                $_SESSION['error'] = "This email is already registrated";
                header("location:index.php");
            } else {
                switch ($_POST['sex']) {
                    case 'male':
                        $avatar = 'images/default_man.png';
                        break;
                    case 'female':
                        $avatar = 'images/default_girl.png';
                        break;
                }
                $control->add_user($_POST['name'], $_POST['surname'], $_POST['sex'], $_POST['email'], $_POST['dob'], password_hash($_POST['pass'], PASSWORD_BCRYPT), $avatar);
                $_SESSION['email'] = $_POST['email'];
                $_SESSION['id'] = $control->get_last_id();
                $control->update_user_info($_SESSION['email'], ['online' => $ses_id]);
                header("location:profile.php");
            }
        } else {
            $_SESSION['error'] = "Please input correct information";
            header("location:index.php");
        }
    } else {
        $_SESSION['error'] = "Please fill all fields";
        header("location:index.php");
    }
} else {
    header("location:index.php");
}