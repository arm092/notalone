<?php
$ses_id = session_id();
if (empty($ses_id)) {
    session_start();
    $ses_id = session_id();
}
if (!isset($_SESSION['id'])) {
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/loading.css">
    <link href="lightbox2-master/dist/css/lightbox.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="shortcut icon" href="images/logo.png" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>Profile Page</title>
</head>

<body class="profile-page">
    <?php
include "controller.php";
$user = $control->get_user_info($_SESSION['id']);
?>
    <main>
        <div class="menu flex">
                <a href="index.php" class="flex">
                    <img src="images/logo_brand.png" id="logo">
                    <div>
                        <h1>NotAlone</h1>
                        <h3>Find a company</h3>
                    </div>
                </a>
            <ul class="flex manage">
                <li class="msg">
                    <img id="msg" class="btn-img" src="images/msg.png">
                    <div class="badge"></div>
                    <div class="messenger">
                        <ul class="flex col">
                        </ul>
                    </div>
                    <div class="chat-box">
                        <ul class="flex col">
                            <li class="chat-with">name</li>
                            <li>
                                <ul class="messages-block flex col">
                                </ul>
                            </li>
                            <li class="new-msg">
                                <div class="flex msg-area">
                                    <textarea type="text" id="send-text"></textarea>
                                    <img id="send" class="btn-img" src="images/send.png" title="Send messages">
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <img id="notif" class="btn-img" src="images/notif.png" title="<?=$user['count_notif']?> new notifications">
                    <div class="badge not-if"></div>
                    <ul class="notif-list flex col">
                    </ul>
                </li>
                <li>
                    <img id="edit" class="btn-img" src="images/account.png" title="Edit account">
                </li>
                <li>
                    <img id="exit" class="btn-img" src="images/exit.png" title="Exit">
                </li>
            </ul>
        </div>
        <div class="flex full profile" style="justify-content: flex-start; padding-top: 4.5vw">
            <div class="profile-wall" data-src="<?=$user['wallpaper']?>">
                <label for="wall" class="upl-btn-img"><img class="change-avatar btn-img" src="images/wall_btn.png"
                        title="Change wallpaper"></label>
                <form action="update.php" method="post" enctype="multipart/form-data" id="wall-form" style="
                    display: none;">
                    <input type="file" name="wallpaper" id="wall">
                </form>
            </div>
            <div class="flex col profile-pic">
                <div data-src="<?=$user['avatar']?>" class="avatar">
                    <label for="avatar" class="upl-btn-img"><img class="change-avatar btn-img" src="images/wall_btn.png"
                            title="Change avatar"></label>
                    <form action="update.php" method="post" enctype="multipart/form-data" id="avatar-form" style="
                    display: none;">
                        <input type="file" name="avatar" id="avatar">
                    </form>
                </div>
            </div>
            <div class="flex col profile-name" style="align-items: start" data-user="<?=$_SESSION['id']?>">
                <h1 class="text-border">
                    <?=$user['name'] . " " . $user['surname'] . ", " . $user['age']?>
                </h1>
            </div>
            <div class="profile-menu flex">
                <button class="orange-btn" data-id="<?=$_SESSION['id']?>" id="info">Information</button>
                <button class="orange-btn" data-id="<?=$_SESSION['id']?>" id="friends">Friends</button>
                <button class="orange-btn" data-id="<?=$_SESSION['id']?>" id="album">Photos</button>
                <button class="orange-btn" data-id="<?=$_SESSION['id']?>" id="feed">Feed</button>
            </div>
        </div>
    </main>
    <section class="info">
        <div class="flex col">
            <h3 class="text-border">Date of Birth: <span class="dob">
                    <?=$user['dob']?></span></h3>
            <h3 class="text-border">Sex: <span class="sex">
                    <?=$user['sex']?></span></h3>
            <h3 class="text-border">Marital status: <span class="m-status">
                    <?=$user['marital_status']?></span></h3>
            <h3 class="text-border">Work at <span class="work-at">
                    <?=$user['work_place']?></span> as <span class="work-as">
                    <?=$user['work_as']?></span></h3>
            <h3 class="text-border">From <span class="city">
                    <?=$user['city']?>,</span> <span class="country">
                    <?=$user['country']?></span></h3>
        </div>
    </section>
    <section class="friends">
        <div class="flex">
            <?php
foreach ($user['friends'] as $friend) {?>
            <div class="friend-block flex col" data-id="<?=$friend['id']?>" data-wall="<?=$friend['wallpaper']?>">
                <div class="friend-ava" data-src="<?=$friend['avatar']?>">
                    <div class="badge-status" data-color="<?=$friend['online'] != '0' ? " limegreen" : "grey"?>"></div>
                </div>
                <h3 class="text-border">
                    <?=$friend['name'] . " " . $friend['surname']?>
                </h3>
            </div>
            <?php }
?>
        </div>
    </section>
    <section class="album">
        <div class="flex">
            <div class="add-photo">
                <label for="add-photo-btn" class="flex col">
                    <img class="btn-img" src="images/add.png">
                    <h3 class="text-border">Add photo</h3>
                </label>
                <form action="update.php" method="post" enctype="multipart/form-data" id="add-photo" style="
                    display: none;">
                    <input type="file" name="new_photo" id="add-photo-btn">
                </form>
            </div>
        </div>
    </section>
    <section class="feed">
        <div class="flex col">
            <button class="new-post btn-large">New post</button>
            <div class="flex feed-block"></div>
        </div>
    </section>
    <div class="modal" id="photo-comment">
        <div class="flex col">
            <h2>Add a description to your photo:</h2>
            <textarea name="description" id="desc" cols="40" rows="10" form="add-photo" maxlength="140"></textarea>
            <button class="orange-btn" form="add-photo">Ok</button>
        </div>
    </div>
    <div class="modal" id="new-post">
        <form class="flex col" action="post.php" method="POST" enctype="multipart/form-data">
            <h2>New post details:</h2>
            <textarea name="post" id="post-text" cols="40" rows="10" placeholder="Your post text" maxlength="140"></textarea>
            <div>
                <label for="upl-picture" class="upl-btn" id="picture-label">Pick a picture</label>
                <input type="file" name="picture" id="upl-picture">
            </div>
            <div class="flex">
                <div class="radio">
                    <input type="radio" name="type" id="r1" value="Invitation">
                    <label for="r1">Invitation</label>
                </div>
                <div class="radio">
                    <input type="radio" name="type" id="r2" value="Offer">
                    <label for="r2">Offer</label>
                </div>
            </div>
            <select name="sex" id="post-sex">
                <option value="" selected disabled>Select foreseen sex</option>
                <option value="unimportantly">Unimportantly</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
            <input type="number" name='age_start' id='post-age-start' placeholder="Input age range: FROM">
            <input type="number" name='age_end' id='post-age-end' placeholder="Input age range: TO">
            <input type="date" name="date" id="post-date" value="<?=date('Y-m-d')?>">
            <div class="flex">
                <button class="cancel orange-btn" type="button">Cancel</button>
                <button id="post-button" class="orange-btn">Post</button>
            </div>
        </form>
    </div>
    <div class="modal" id="update">
        <form action="update.php" method="POST" class="flex col">
            <h2>Update your information</h2>
            <div class="flex">
                <div class="flex col">
                    <input type="text" name="name" placeholder="Your name" value="<?=$user['name']?>">
                    <input type="text" name="surname" placeholder="Your surname" value="<?=$user['surname']?>">
                    <input type="date" name="dob" value="<?=$user['date_of_birth']?>">
                    <input type="password" name="pass" placeholder="Change password" id="password">
                    <input type="password" name="confirm" placeholder="Confirm new password">
                </div>
                <div class="flex col">
                    <select name="marital_status">
                        <option value="" selected disabled>Your marital status</option>
                        <option value="Single" <?=$user['marital_status'] == "Single" ? 'selected' : ''?>>Single</option>
                        <option value="In the relations" <?=$user['marital_status'] == "In the relations" ? 'selected' :
''?>>In a relationship</option>
                        <option value="Betrothed" <?=$user['marital_status'] == "Betrothed" ? 'selected' : ''?>>Betrothed</option>
                        <option value="Married" <?=$user['marital_status'] == "Married" ? 'selected' : ''?>>Married</option>
                        <option value="Widowed" <?=$user['marital_status'] == "Widowed" ? 'selected' : ''?>>Widowed</option>
                    </select>
                    <input type="text" name="work_place" placeholder="Your place of work" value="<?=$user['work_place']?>">
                    <input type="text" name="work_as" placeholder="Your position at work" value="<?=$user['work_as']?>">
                    <input type="text" name="city" placeholder="Your city" autocomplete="city" value="<?=$user['city']?>">
                    <input type="text" name="country" placeholder="Your country" autocomplete="country" value="<?=$user['country']?>">
                </div>
            </div>
            <div class="flex">
                <label for="pass">Type your password for save: </label>
                <input type="password" name="old" id="pass" required>
            </div>
            <div class="flex">
                <button class="cancel orange-btn" type="button">Cancel</button>
                <button class="orange-btn">Save</button>
            </div>
        </form>
    </div>
    <div class="modal" id="request">
        <div class="flex col">
            <div class="flex f-wall">
                <div class="avatar f-avatar" data-src="">
                    <div class="badge-status"></div>
                </div>
            </div>
            <h2 id="text"></h2>
            <div class="flex">
                <button class="cancel orange-btn" type="button">Cancel</button>
                <button class="r-not orange-btn">Decline</button>
                <button class="r-ok orange-btn">Accept</button>
            </div>
        </div>
    </div>
    <div class="modal" id="response">
        <div class="flex col">
            <h1></h1>
            <div class="picture">
            </div>
            <h2 class="post-text"></h2>
            <h3 id="date"></h3>
            <div class="flex"><button class="cancel orange-btn" type="button">Cancel</button></div>
        </div>
    </div>
    <?php
if (isset($_SESSION['error'])) {
    ?>
    <div class="error-box">
        <?=$_SESSION['error']?>
    </div>
    <?php
unset($_SESSION['error']);
}
?>
    <script src="js/script.js">
    </script>
    <script src="lightbox2-master/dist/js/lightbox.js">
    </script>
    <script>
        lightbox.option({
            'resizeDuration': 500,
            'wrapAround': true,
            'positionFromTop': 15
        })
    </script>
</body>

</html>