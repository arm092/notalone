<?php
include "db.php";
class Controller extends Database
{
    public function __construct($database)
    {
        parent::__construct($database);
    }
    public function add_user($name, $surname, $sex, $email, $dob, $pass, $avatar)
    {
        $this->insert("users", ['name' => $name, 'surname' => $surname, 'sex' => $sex, 'email' => $email, 'date_of_birth' => $dob, 'password' => $pass, 'avatar' => $avatar]);
    }
    public function post($user_id, $post)
    {
        $post['user_id'] = $user_id;
        $post['date'] = strtotime($post['date']);
        $this->insert('posts', $post);
    }
    public function get_feed($user_id)
    {
        $user = $this->select('users', ['id' => $user_id], '')->fetch_array(MYSQLI_ASSOC);
        $posts = $this->select_posts($user_id, $user['sex'], $user['date_of_birth'])->fetch_all(MYSQLI_ASSOC);
        $friends_id = $this->select('friends', ['user_id' => $user['id'], 'friend_id' => $user['id']], 'OR')->fetch_all(true);
        $friends = [];
        foreach ($friends_id as $f_id) {
            $friend = $f_id['user_id'] == $user['id'] ? $f_id['friend_id'] : $f_id['user_id'];
            $friends = array_merge($friends, $this->select('photos', ['user_id' => $friend], '')->fetch_all(MYSQLI_ASSOC));
        }
        shuffle($friends);
        return ['posts' => $posts, 'photos' => $friends];
    }
    public function get_user_info($id)
    {
        $user_info = $this->select('users', ['id' => $id], '')->fetch_array(MYSQLI_ASSOC);
        $age = date('Y') - date('Y', strtotime($user_info['date_of_birth']));
        $user_info['age'] = $age;
        $user_info['dob'] = date('d.m.Y', strtotime($user_info['date_of_birth']));
        $user_info['notif'] = $this->select('notifications', ['user_id' => $user_info['id'], 'status' => 1], 'AND')->fetch_all(true);
        $user_info['count_notif'] = count($user_info['notif']);
        $friends_id = $this->select('friends', ['user_id' => $user_info['id'], 'friend_id' => $user_info['id']], 'OR')->fetch_all(true);
        $user_info['count_friends'] = count($friends_id);
        $friends = [];
        foreach ($friends_id as $f_id) {
            $friend = $f_id['user_id'] == $user_info['id'] ? $f_id['friend_id'] : $f_id['user_id'];
            $friends[] = $this->select('users', ['id' => $friend], '')->fetch_array(MYSQLI_ASSOC);
        }
        $user_info['friends'] = $friends;
        return $user_info;
    }
    public function get_all_users()
    {
        return $this->select('users', [], '')->fetch_all(true);
    }
    public function get_emails()
    {
        return $this->select('users', [], '', 'email')->fetch_all(true);
    }
    public function check_user($email, $pass)
    {
        $check = $this->select('users', ['email' => $email], '', 'id, password')->fetch_array(MYSQLI_ASSOC);
        if (!empty($check) && password_verify($pass, $check['password'])) {
            return $check;
        } else {
            return [];
        }
        //return !empty($check);
    }
    public function update_user_info($email, $update)
    {
        $this->update('users', ['email' => $email], '', $update);
    }
    public function add_photo($id, $photo_dir, $description, $time)
    {
        $this->insert('photos', ['photo' => $photo_dir, 'user_id' => $id, 'description' => $description, 'add_time' => $time]);
    }
    public function get_notifications($id)
    {
        $notifs = $this->select('notifications', ['user_id' => $id, 'status' => 1], 'AND')->fetch_all(true);
        $post = [];
        $from = [];
        foreach ($notifs as $index => $notif) {
            switch ($notif['category']) {
                case 'response':
                    $post_id = $notif['post_id'];
                    $post = $this->select('posts', ['id' => $post_id], '')->fetch_array(MYSQLI_ASSOC);
                    $from = $this->select('users', ['id' => $notif['from_id']], '')->fetch_array(MYSQLI_ASSOC);
                    $notifs[$index] = array_merge($notif, ['post' => $post, 'from' => $from]);
                    break;
                case 'like':
                    $post_id = $notif['post_id'];
                    $post = $this->select('photos', ['id' => $post_id], '')->fetch_array(MYSQLI_ASSOC);
                    $from = $this->select('users', ['id' => $notif['from_id']], '')->fetch_array(MYSQLI_ASSOC);
                    $notifs[$index] = array_merge($notif, ['post' => $post, 'from' => $from]);
                    break;
                default:
                    $notifs[$index] = array_merge($notif, ['post' => [], 'from' => []]);
                    break;
            }
        }
        return array_reverse($notifs);
    }
    public function get_user_contacts($user_id)
    {
        $msgs = $this->select('messages', ['from_id' => $user_id, 'to_id' => $user_id], 'OR', '*', ' ORDER BY status DESC')->fetch_all(true);
        $contacts = [];
        foreach ($msgs as $msg) {
            $contact = $msg['from_id'] == $user_id ? $msg['to_id'] : $msg['from_id'];
            $contacts[] = $this->select('users', ['id' => $contact], '')->fetch_array(MYSQLI_ASSOC);
        }
        return array_unique($contacts, SORT_REGULAR);
    }
    public function check_unread_msgs($from_id, $to_id)
    {
        $msgs = $this->select('messages', ['from_id' => $from_id, 'to_id' => $to_id, 'status' => 1], 'AND')->fetch_all(true);
        return !empty($msgs);
    }
    public function get_user_msgs($user_id, $chater, $limit)
    {
        $msgs = $this->select_msgs($user_id, $chater, $limit)->fetch_all(true);
        $this->update('messages', ['from_id' => $chater, 'to_id' => $user_id, 'status' => 1], 'AND', ['status' => 0]);
        return array_reverse($msgs);
    }
    public function check_unread_badge($to_id)
    {
        $msgs = $this->select('messages', ['to_id' => $to_id, 'status' => 1], 'AND')->fetch_all(true);
        return !empty($msgs);
    }
    public function get_user_posts($id)
    {
        return $this->select('posts', ['user_id' => $id], '')->fetch_all(true);
    }
    public function get_user_photos($id)
    {
        return $this->select('photos', ['user_id' => $id], '')->fetch_all(true);
    }
    public function get_photo($id)
    {
        return $this->select('photos', ['id' => $id], '')->fetch_array(MYSQLI_ASSOC);
    }
    public function get_likes_count($id)
    {
        return count($this->select('likes', ['photo_id' => $id], '')->fetch_all(MYSQLI_ASSOC));
    }
    public function get_responses_count($id)
    {
        return count($this->select('responses', ['post_id' => $id], '')->fetch_all(MYSQLI_ASSOC));
    }
    public function like($photo_id, $liker_id)
    {
        $already = $this->select('likes', ['photo_id' => $photo_id, 'liker_id' => $liker_id], 'AND')->fetch_all(MYSQLI_ASSOC);
        if (empty($already)) {
            $this->insert('likes', ['photo_id' => $photo_id, 'liker_id' => $liker_id]);
            $user_id = $this->select('photos', ['id' => $photo_id], '')->fetch_array(MYSQLI_ASSOC)['user_id'];
            if ($user_id != $liker_id) {
                $this->insert('notifications', ['user_id' => $user_id, 'from_id' => $liker_id, 'category' => 'like', 'post_id' => $photo_id]);
            }
        }
        return empty($already);
    }
    public function response($post_id, $responser)
    {
        $already = $this->select('responses', ['post_id' => $post_id, 'responser' => $responser], 'AND')->fetch_all(MYSQLI_ASSOC);
        if (empty($already)) {
            $this->insert('responses', ['post_id' => $post_id, 'responser' => $responser]);
            $user = $this->select('posts', ['id' => $post_id], '')->fetch_array(MYSQLI_ASSOC);
            $user_id = $user['user_id'];
            $this->insert('notifications', ['user_id' => $user_id, 'from_id' => $responser, 'category' => 'response', 'post_id' => $post_id]);
        }
        return empty($already);
    }
    public function send_msg($from_id, $to_id, $msg)
    {
        $this->insert('messages', ['message' => $msg, 'from_id' => $from_id, 'to_id' => $to_id, 'date' => time()]);
    }
    public function del_photo($id)
    {
        $photo = $this->select('photos', ['id' => $id], '')->fetch_array(MYSQLI_ASSOC);
        $user = $this->select('users', ['id' => $photo['user_id']], '')->fetch_array(MYSQLI_ASSOC);
        if ($photo['photo'] == $user['avatar'] && $user['sex'] == 'male') {
            $this->update('users', ['id' => $user['id']], '', ['avatar' => 'images/default_man.png']);
        } elseif ($photo['photo'] == $user['avatar'] && $user['sex'] == 'female') {
            $this->update('users', ['id' => $user['id']], '', ['avatar' => 'images/default_girl.png']);
        }
        unlink($photo['photo']);
        $this->delete('photos', ['id' => $id], '');
    }
    public function read_notif($id)
    {
        $this->update('notifications', ['id' => $id], '', ['status' => 0]);
    }
    public function is_friend($user_id, $friend_id)
    {
        $check = $this->select('friends', ['user_id' => $user_id, 'friend_id' => $friend_id], 'AND', 'id')->fetch_all(true);
        return !empty($check);
    }
    public function remove_from_friends($user_id, $friend_id)
    {
        $this->delete('friends', ['user_id' => $user_id, 'friend_id' => $friend_id], 'AND');
    }
    public function send_friend_request($from, $to)
    {
        $this->insert('notifications', ['user_id' => $to, 'from_id' => $from, 'category' => 'request', 'post_id' => 0]);
    }
    public function withdraw_friend_request($from, $to)
    {
        $this->delete('notifications', ['user_id' => $to, 'from_id' => $from, 'category' => 'request'], 'AND');
    }
    public function accept_request($id)
    {
        $request = $this->select('notifications', ['id' => $id], '')->fetch_array(MYSQLI_ASSOC);
        $this->insert('friends', ['user_id' => $request['from_id'], 'friend_id' => $request['user_id']]);
        $this->delete('notifications', ['id' => $id], '');
    }
    public function decline_request($id)
    {
        $this->delete('notifications', ['id' => $id], '');
    }
    public function check_request($user_id, $from_id)
    {
        $check = $this->select('notifications', ['user_id' => $user_id, 'from_id' => $from_id, 'category' => 'request'], 'AND')->fetch_all(true);
        return !empty($check);
    }
    public function get_last_id()
    {
        return $this->last_id();
    }
}
$control = new Controller("notalone");