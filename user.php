<?php
$ses_id = session_id();
if (empty($ses_id)) {
    session_start();
    $ses_id = session_id();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $_SESSION['user_id'] = $_POST['user_id'];
    header("location:index.php");
}
if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == $_SESSION['id']) {
    header("location:index.php");
}
include "controller.php";
$user = $control->get_user_info($_SESSION['id']);
$friend = $control->get_user_info($_SESSION['user_id']);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/loading.css">
    <link href="lightbox2-master/dist/css/lightbox.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="shortcut icon" href="images/logo.png" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title><?=$friend['name'] . " " . $friend['surname']?></title>
</head>

<body class="profile-page">
    <main>
        <div class="menu flex">
            <div class="flex">
                <a href="index.php" class="flex">
                    <img src="images/logo_brand.png" id="logo">
                    <div>
                        <h1>NotAlone</h1>
                        <h3>Find a company</h3>
                    </div>
                </a>
            </div>
            <ul class="flex manage">
                <li class="msg">
                    <img id="msg" class="btn-img" src="images/msg.png">
                    <div class="badge"></div>
                    <div class="messenger">
                        <ul class="flex col">
                        </ul>
                    </div>
                    <div class="chat-box">
                        <ul class="flex col">
                            <li class="chat-with">name</li>
                            <li>
                                <ul class="messages-block flex col">
                                </ul>
                            </li>
                            <li class="new-msg">
                                <div class="flex msg-area">
                                    <textarea type="text" id="send-text"></textarea>
                                    <img id="send" class="btn-img" src="images/send_blue.png" title="Send messages">
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <img id="notif" class="btn-img" src="images/notif.png" title="<?=$user['count_notif']?> new notifications">
                    <div class="badge not-if"></div>
                    <ul class="notif-list flex col">
                    </ul>
                </li>
                <li>
                    <img id="edit" class="btn-img" src="images/account.png" title="Edit account">
                </li>
                <li>
                    <img id="exit" class="btn-img" src="images/exit.png" title="Exit">
                </li>
            </ul>
        </div>
        <div class="flex full profile" style="justify-content: flex-start; padding-top: 4.5vw">
            <div class="profile-wall" data-src="<?=$friend['wallpaper']?>">
            </div>
            <div class="flex col profile-pic">
                <div data-src="<?=$friend['avatar']?>" class="avatar">
                    <div class="badge-status"></div>
                </div>
            </div>
            <div class="flex col profile-name" style="align-items: start" data-friend="<?=$_SESSION['user_id']?>"
                data-user="<?=$_SESSION['id']?>">
                <h1 class="text-border">
                    <?=$friend['name'] . " " . $friend['surname'] . ", " . $friend['age']?>
                </h1>
            </div>
            <div class="profile-menu flex">
                <?php
$is_friend = $control->is_friend($_SESSION['id'], $_SESSION['user_id']) || $control->is_friend($_SESSION['user_id'], $_SESSION['id']);
$is_sent = $control->check_request($_SESSION['user_id'], $_SESSION['id']);
if (!$is_sent && $is_friend) {
    ?>
                <script>
                    $('.badge-status').show().css({
                                    backgroundColor: <?=$friend['online'] != '0'?> > 0 ? 'limegreen' : 'grey'
                                })
                </script>
                <button class="orange-btn" data-id="<?=$_SESSION['user_id']?>" id="info">Information</button>
                <button class="orange-btn" data-id="<?=$_SESSION['user_id']?>" id="friends">Friends</button>
                <button class="orange-btn" data-id="<?=$_SESSION['user_id']?>" id="album">Photos</button>
                <button class="orange-btn" data-id="<?=$_SESSION['user_id']?>" id="posts">Posts</button>
                <button class="orange-btn" data-id="<?=$_SESSION['user_id']?>" id="send-msg">Send message</button>
                <button class="red-btn" data-id="<?=$_SESSION['user_id']?>" id="stop">Stop friendship</button>

                <?php
} elseif ($is_sent) {
    ?>
                <script>
                    $('.badge-status').hide()
                </script>
                <h2 class="fr-inf">You sent friend request</h2>
                <button class="orange-btn" data-id="<?=$_SESSION['user_id']?>" id="withdraw">Withdraw request</button>
                <button class="orange-btn" data-id="<?=$_SESSION['user_id']?>" id="send-msg">Send message</button>
                <?php
} else {
    ?>
                <script>
                    $('.badge-status').hide()
                </script>
                <h2 class="fr-inf">You are not friends</h2>
                <button class="orange-btn" data-id="<?=$_SESSION['user_id']?>" id="add-friend">Add to friends</button>
                <button class="orange-btn" data-id="<?=$_SESSION['user_id']?>" id="send-msg">Send message</button>
                <?php
}
?>
            </div>
        </div>
    </main>
    <section class="info">
        <div class="flex col">
            <h3 class="text-border">Date of Birth: <span class="dob">
                    <?=$friend['dob']?></span></h3>
            <h3 class="text-border">Sex: <span class="sex">
                    <?=$friend['sex']?></span></h3>
            <h3 class="text-border">Marital status: <span class="m-status">
                    <?=$friend['marital_status']?></span></h3>
            <h3 class="text-border">Work at <span class="work-at">
                    <?=$friend['work_place']?></span> as <span class="work-as">
                    <?=$friend['work_as']?></span></h3>
            <h3 class="text-border">From <span class="city">
                    <?=$friend['city']?>,</span> <span class="country">
                    <?=$friend['country']?></span></h3>
        </div>
    </section>
    <section class="friends">
        <div class="flex">
            <?php
foreach ($friend['friends'] as $frien) {?>
            <div class="friend-block flex col" data-id="<?=$frien['id']?>" data-wall="<?=$frien['wallpaper']?>">
                <div class="friend-ava" data-src="<?=$frien['avatar']?>">
                    <div class="badge-status" data-color="<?=$frien['online'] != '0' ? " limegreen" : "grey"?>"></div>
                </div>
                <h3 class="text-border">
                    <?=$frien['name'] . " " . $frien['surname']?>
                </h3>
            </div>
            <?php }
?>
        </div>
    </section>
    <section class="album">
        <div class="flex">

        </div>
    </section>
    <section class="posts">
        <div class="flex feed-block"></div>
    </section>
    <div class="modal" id="update">
        <form action="update.php" method="POST" class="flex col">
            <h2>Update your information</h2>
            <div class="flex">
                <div class="flex col">
                    <input type="text" name="name" placeholder="Your name" value="<?=$user['name']?>">
                    <input type="text" name="surname" placeholder="Your surname" value="<?=$user['surname']?>">
                    <input type="date" name="dob" value="<?=$user['date_of_birth']?>">
                    <input type="password" name="pass" placeholder="Change password" id="password">
                    <input type="password" name="confirm" placeholder="Confirm new password">
                </div>
                <div class="flex col">
                    <select name="marital_status">
                        <option value="" selected disabled>Your marital status</option>
                        <option value="Single" <?=$user['marital_status'] == "Single" ? 'selected' : ''?>>Single</option>
                        <option value="In the relations" <?=$user['marital_status'] == "In the relations" ? 'selected' :
''?>>In a relationship</option>
                        <option value="Betrothed" <?=$user['marital_status'] == "Betrothed" ? 'selected' : ''?>>Betrothed</option>
                        <option value="Married" <?=$user['marital_status'] == "Married" ? 'selected' : ''?>>Married</option>
                        <option value="Widowed" <?=$user['marital_status'] == "Widowed" ? 'selected' : ''?>>Widowed</option>
                    </select>
                    <input type="text" name="work_place" placeholder="Your place of work" value="<?=$user['work_place']?>">
                    <input type="text" name="work_as" placeholder="Your position at work" value="<?=$user['work_as']?>">
                    <input type="text" name="city" placeholder="Your city" autocomplete="city" value="<?=$user['city']?>">
                    <input type="text" name="country" placeholder="Your country" autocomplete="country" value="<?=$user['country']?>">
                </div>
            </div>
            <div class="flex">
                <label for="pass">Type your password for save: </label>
                <input type="password" name="old" id="pass" required>
            </div>
            <div class="flex">
                <button class="cancel orange-btn" type="button">Cancel</button>
                <button class="orange-btn">Save</button>
            </div>
        </form>
    </div>
    <div class="modal" id="request">
        <div class="flex col">
            <div class="flex f-wall">
                <div class="avatar f-avatar" data-src="">
                    <div class="badge-status"></div>
                </div>
            </div>
            <h2 id="text"></h2>
            <div class="flex">
                <button class="cancel orange-btn" type="button">Cancel</button>
                <button class="r-not orange-btn">Decline</button>
                <button class="r-ok orange-btn">Accept</button>
            </div>
        </div>
    </div>
    <div class="modal" id="response">
        <div class="flex col">
            <h1></h1>
            <div class="picture">
            </div>
            <h2 class="post-text"></h2>
            <h3 id="date"></h3>
            <div class="flex"><button class="cancel orange-btn" type="button">Cancel</button></div>
        </div>
    </div>
    <div class="modal" id="new-msg">
        <div class="flex col">
            <h2>Send message to
                <?=$friend['name']?>
                <?=$friend['surname']?>:
            </h2>
            <textarea name="message" id="msg-text" cols="40" rows="10" maxlength="140" placeholder="Message"></textarea>
            <div class="flex">
                <button class="cancel orange-btn" type="button">Cancel</button>
                <button class="orange-btn" id="send-msg-ok">Send</button>
            </div>
        </div>
    </div>
    <?php
if (isset($_SESSION['error'])) {
    ?>
    <div class="error-box">
        <?=$_SESSION['error']?>
    </div>
    <?php
unset($_SESSION['error']);
}
?>
    <script src="js/script.js">
    </script>
    <script src="lightbox2-master/dist/js/lightbox.js">
    </script>
    <script>
        lightbox.option({
            'resizeDuration': 500,
            'wrapAround': true,
            'positionFromTop': 15
        })
    </script>
</body>

</html>