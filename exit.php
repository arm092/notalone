<?php
session_start();
if (isset($_SESSION['email'])) {
    include "controller.php";
    $control->update_user_info($_SESSION['email'], ['online' => '0']);
    session_unset();
    session_destroy();
}
header('location:index.php');