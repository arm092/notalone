<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("location:index.php");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['post']) && !empty($_POST['post']) && isset($_POST['type']) && !empty($_POST['type']) && isset($_POST['age_start']) && !empty($_POST['age_start']) && filter_var($_POST['age_start'], FILTER_VALIDATE_INT) && isset($_POST['age_end']) && !empty($_POST['age_end']) && filter_var($_POST['age_end'], FILTER_VALIDATE_INT) && isset($_POST['sex']) && !empty($_POST['sex']) && isset($_POST['date']) && !empty($_POST['date']) && strtotime($_POST['date'])) {
        include "controller.php";
        $post = $_POST;
        $post['post_time'] = time();
        if (!empty($_FILES['picture']) && !$_FILES['picture']['error']) {
            if (!file_exists('images/posts/' . $_SESSION['id'])) {
                mkdir('images/posts/' . $_SESSION['id']); //create images folder, if it not exist
            }
            $upload_dir = 'images/posts/' . $_SESSION['id'] . '/' . uniqid() . $_FILES['picture']['name'];
            move_uploaded_file($_FILES['picture']['tmp_name'], $upload_dir);
            $post['picture'] = $upload_dir;
        } else {
            $post['picture'] = 'images/posts/default.png';
        }
        $control->post($_SESSION['id'], $post);
    } else {
        $_SESSION['error'] = "Please input correct data";
    }
}
header('location:profile.php');