function avatar() {
    $('*[data-src]').each(function() {
        let _this = $(this)
        _this.css('background-image', 'url("' + _this.attr('data-src') + '")')
    })
    $('*[data-color]').each(function() {
        let _this = $(this)
        _this.css('background-color', _this.attr('data-color'))
    })
}

function unread() {
    $('.unread').css({
        color: 'rgb(250,250,255)'
    })
    setTimeout(() => {
        $('.unread').css({
            color: '#ec8213'
        })
    }, 350);
}
setInterval(unread, 700)

function sendMessage() {
    let to = $(this).attr('data-id');
    let msg = $('#msg-text').val();
    let action = 'msg';
    $.post({
        url: 'manage.php',
        data: {
            action,
            to,
            msg
        },
        success: function() {
            $('#new-msg').fadeOut()
        }
    })
}

function addMsg(message) {
    let li = $('<li></li>')
    let timeText = '';
    let time = Math.round((Date.now() - +message.date * 1000) / 1000);
    if (time < 60) {
        timeText = Math.round(time) + ' seconds ago';
    } else {
        time /= 60
        if (time < 60) {
            timeText = Math.round(time) + ' minute(s) ago';
        } else {
            time /= 60
            if (time < 24) {
                timeText = Math.round(time) + ' hour(s) ago';
            } else {
                time /= 24
                timeText = Math.round(time) + ' day(s) ago';
            }
        }
    }
    li.text(message.message).attr({
        'data-id': message.id,
        'title': timeText
    })
    if (message.from_id == userId) li.addClass('msg-out')
    else li.addClass('msg-in')
    li.appendTo('.messages-block')
}

function loadMsgs(chater, count) {
    $.post({
        url: 'manage.php',
        dataType: 'json',
        data: {
            action: 'get_msgs',
            id: userId,
            chater_id: chater,
            limit: msgsCount + count
        },
        success: function(res) {
            $('.messages-block').empty();
            for (const msg in res) {
                if (res.hasOwnProperty(msg)) {
                    const message = res[msg];
                    addMsg(message)
                }
            }
            if (msgsCount == 0) $('.messages-block').scrollTop($('.messages-block').prop('scrollHeight'))
            msgsCount += count;
        }
    })
}

function chating() {
    let socket = new WebSocket('ws://localhost:8080');

    // open connection
    socket.onopen = function(e) {
        $('#send').attr('src', 'images/send_blue.png')
    }

    // close connection
    socket.onclose = function(e) {
        $('#send').attr('src', 'images/send.png')
        if (e.wasClean) {
            console.log("Disconected");
        } else {
            console.log("Disconected not normal");
            console.log("code: " + e.code + " reason: " + e.reason);
        }
    }

    // get data
    socket.onmessage = function(e) {
        let msg = JSON.parse(e.data);
        if (msg.to_id == userId || msg.from_id == userId) addMsg(msg)
        $('.messages-block').scrollTop($('.messages-block').prop('scrollHeight'))
    }

    // error
    socket.onerror = function(e) {
        console.log("erorr: " + e.message)
    }

    // send data
    function sendData() {
        let message = {
            from_id: userId,
            to_id: $('.chat-with').attr('data-id'),
            message: $('#send-text').val(),
            date: Math.round(Date.now() / 1000),
            session: $('.chat-with').attr('data-session')
        }
        socket.send(JSON.stringify(message));
        $.post({
            url: 'manage.php',
            data: {
                action: 'msg',
                to: message.to_id,
                msg: message.message
            },
            success: function() {
                $('#send-text').val('')
            }
        })
    }
    $('#send-text').keypress(function(key) {
        if (key.which == 13) {
            key.preventDefault()
        }
        if (key.keyCode == 13) {
            sendData()
        }
    })
    $('#send').on('click', sendData)
}

function removeFromFriends() {
    if (confirm('Are you sure?')) {
        let id = $(this).attr('data-id')
        $.post({
            url: 'manage.php',
            data: {
                action: 'f_remove',
                id
            },
            success: function() {
                location.reload();
            }
        })
    } else return
}

function withdrawRequest() {
    $(this).text('Add to friends').on('click', addToFriends)
    let id = $(this).attr('data-id')
    $.post({
        url: 'manage.php',
        data: {
            action: 'f_withdraw',
            id
        },
        success: function() {}
    })
}

function addToFriends() {
    $(this).text('Withdraw request').off().on('click', withdrawRequest)
    let id = $(this).attr('data-id')
    $.post({
        url: 'manage.php',
        data: {
            action: 'f_request',
            id
        },
        success: function() {}
    })
}

function getNotifications() {
    $.post({
        url: 'manage.php',
        dataType: 'json',
        data: {
            action: 'get_notifications',
            id: userId
        },
        success: function(res) {
            $('.notif-list').empty();
            if (res.length > 0) {
                $('#notif').attr('title', res.length + ' new notifications')
                $('.not-if').fadeIn()
            } else {
                $('#notif').attr('title', 'There are not new notifications')
                $('.notif-list').hide('slide');
                $('.not-if').fadeOut()
            }
            for (const notif of res) {
                let li = $(`<li data-id="${notif.id}" data-type="${notif.category}" data-from="${notif.from_id}"></li>`);
                switch (notif.category) {
                    case 'like':
                        li.html(`<a class="user-link" href="javascript:void(0)" data-id="${notif.from_id}">${notif.from.name} ${notif.from.surname} </a>
                        liked your <a class="like-link" data-lightbox="album" href="${notif.post.photo}" data-title="${notif.post.description}" data-id="${notif.post.id}">photo</a><div class="badge read" title="Mark as read"></div>`)
                        break;
                    case 'response':
                        li.html(`<a class="user-link" href="javascript:void(0)" data-id="${notif.from_id}">${notif.from.name} ${notif.from.surname} </a> responded to your <a class="response-link" data-text='${notif.post.post}' data-date="${notif.post.date}" data-picture="${notif.post.picture}" href="javascript:void(0)">${notif.post.type}</a><div class="badge read" title="Mark as read"></div>`)
                        break;
                    case 'request':
                        li.html(`<a class="request-link" href="javascript:void(0)" data-id="${notif.from_id}">${notif.from.name} ${notif.from.surname} sent you a friend request</a><div class="badge read" title="Mark as read"></div>`)
                        break;
                    default:
                        break;
                }
                li.appendTo('.notif-list');
            }
        }
    })
}

function readNotif(notif) {
    let id = notif.attr('data-id')
    $.post({
        url: 'manage.php',
        data: {
            action: 'read',
            id
        },
        success: function() {
            notif.find('*').css({
                color: 'rgba(8, 72, 135, .7)'
            })
            notif.find('.read').fadeOut()
        }
    })
}

function openUserPage() {
    let id = $(this).attr('data-id');
    $.post({
        url: 'user.php',
        data: {
            user_id: id
        },
        success: function() {
            location.href = 'user.php'
        }
    })
}

let avatarInterval = setInterval(avatar, 1500);
let notifInterval = setInterval(getNotifications, 1000);
let userId = $('.profile-name').attr('data-user');
let msgsCount = 0;
$(document).ready(function() {
    if (userId) chating()
    avatar()
    getNotifications();
    if (localStorage.getItem('reg')) {
        data = JSON.parse(localStorage.reg)
        $('#reg-form .name').val(data.name)
        $('#reg-form .surname').val(data.surname)
        $('#reg-form .dob').val(data.dob)
        $('#reg-form .sex').val(data.sex)
        $('#reg-form .email').val(data.email)
        localStorage.clear();
    }

    $('#notif').on('click', function() {
        if ($('.notif-list li').length > 0) $('.notif-list').toggle('slide')
    })

    $(document).on('click', '.user-link', openUserPage)

    $(document).on('click', '.response-link', function() {
        let type = $(this).text();
        let text = $(this).attr('data-text');
        let date = $(this).attr('data-date');
        let pic = $(this).attr('data-picture');
        $('.modal').fadeOut()
        $('#response').fadeIn();
        $('#response h1').text(type);
        $('#response .picture').attr('data-src', pic);
        $('#response h2').text(text)
        let time = new Date(date * 1000)
        date = (time.getDate() < 10 ? "0" : "") + time.getDate() + "." + ((1 + time.getMonth()) < 10 ? "0" : "") + (1 + time.getMonth()) + "." + time.getFullYear();
        $('#response h3').text(date)
        readNotif($(this).parent())
    })

    $(document).on('mousedown', '.like-link', function() {
        $('section:not(.album)').slideUp()
        $('.album').slideDown(200);
        showAlbum(userId)
        readNotif($(this).parent())
    })

    $(document).on('click', '.request-link', function() {
        let from = $(this).attr('data-from')
        let id = $(this).attr('data-id')
        let text = $(this).find('a').text()
        let _this = $(this)
        $.post({
            url: 'manage.php',
            dataType: 'json',
            data: {
                action: 'get_user',
                id: from
            },
            success: function(res) {
                $('#request').fadeIn()
                $('#text').text(text)
                $('.f-avatar').attr('data-src', res.avatar)
                $('.f-wall').attr('data-src', res.wallpaper)
                $('.r-ok').one('click', function() {
                    $.post({
                        url: 'manage.php',
                        data: {
                            action: 'accept',
                            id
                        },
                        success: function() {
                            $('#request').fadeOut()
                            $('.notif-list').hide('slide')
                            _this.remove();
                            if ($('section.friends').is(':hidden')) {
                                showFriends();
                            } else {
                                showFriends()
                                showFriends()
                            }
                        }
                    })
                    readNotif(_this.parent());
                })
                $('.r-not').one('click', function() {
                    $.post({
                        url: 'manage.php',
                        data: {
                            action: 'decline',
                            id
                        },
                        success: function() {
                            $('#request').fadeOut()
                            $('.notif-list').hide('slide')
                            _this.remove();
                        }
                    })
                    readNotif(_this.parent());
                })
            }
        })
    })

    $('.notif-list li .read').click(function() {
        readNotif($(this).parent())
    })


    // ***SEARCH FUNCTIONS***

    // function searchShow() {
    //     $('.find').show();
    //     $('.search').show('slide')
    //     $('#search').one('click', searchHide)
    // }

    // function searchHide() {
    //     $('.search').hide('slide')
    //     $('.find').hide(500);
    //     $('#search').one('click', searchShow)
    // }
    // $('#search').one('click', searchShow)
    // $('.search').on('input', function() {
    //     if ($(this).val() != '') {
    //         $('.results').show('slide')
    //         let value = $(this).val()
    //         $.post({
    //             url: 'manage.php',
    //             dataType: 'json',
    //             data: {
    //                 action: 'search',
    //                 value
    //             },
    //             success: function(res) {
    //                 $('.results').empty();
    //                 for (const user of res) {
    //                     if (user.id == userId) continue
    //                     let li = $(`<li data-id="${user.id}" data-wall="${user.wallpaper}"></li>`)
    //                     li.html(`<img src="${user.avatar}" class="search-avatar"><h4>${user.name} ${user.surname}</h4>`).appendTo('.results').one('click', showFriendModal)
    //                 }
    //             }
    //         })
    //     } else {
    //         $('.results').hide('slide')
    //     }
    // });


    $('.new-post').on('click', function() {
        $('#new-post').fadeIn();
    })
    let feed = [];

    function refreshFeed() {
        $('.feed-block').empty();
        $('.loading').remove()
        $('.load-more').remove()
        $('.feed>div').append(`<div class="loading">
        <div class="finger finger-1">
          <div class="finger-item">
            <span></span><i></i>
          </div>
        </div>
                    <div class="finger finger-2">
          <div class="finger-item">
            <span></span><i></i>
          </div>
        </div>
                    <div class="finger finger-3">
          <div class="finger-item">
            <span></span><i></i>
          </div>
        </div>
                    <div class="finger finger-4">
          <div class="finger-item">
            <span></span><i></i>
          </div>
        </div>
                    <div class="last-finger">
          <div class="last-finger-item"><i></i></div>
        </div>
              </div>`)
        $.post({
            url: 'manage.php',
            dataType: 'json',
            data: {
                action: 'get_feed',
                id: userId
            },
            success: function(res) {
                feed = res;
                for (const post of feed.posts) {
                    $.post({
                        url: 'manage.php',
                        dataType: 'json',
                        data: {
                            action: 'get_user',
                            id: post.user_id
                        },
                        success: function(res) {
                            post.user_id = res;
                        }
                    })
                    $.post({
                        url: 'manage.php',
                        dataType: 'json',
                        data: {
                            action: 'get_responses',
                            resp: post.id
                        },
                        success: function(res) {
                            post.responses = res.count;
                        }
                    })
                }
                for (const photo of feed.photos) {
                    $.post({
                        url: 'manage.php',
                        dataType: 'json',
                        data: {
                            action: 'get_user',
                            id: photo.user_id
                        },
                        success: function(res) {
                            photo.user_id = res;
                        }
                    })
                    $.post({
                        url: 'manage.php',
                        dataType: 'json',
                        data: {
                            action: 'get_likes',
                            photo: photo.id
                        },
                        success: function(res) {
                            photo.likes = res.count;
                        }
                    })
                }
                setTimeout(() => {
                    $('.loading').slideUp().remove()
                    loadFeed()
                    avatar()
                    $('<button class="btn-large load-more">Load more...</button>').appendTo('.feed>div')
                    $(document).on('click', '.load-more', loadFeed)
                }, 6000);
            }
        })
    }

    function loadFeed() {
        $('.loading').slideUp().remove()
        for (let feedCount = 0; feedCount < 10; feedCount++) {
            let post = {}
            if (Math.round(Math.random())) {
                //posts
                if (feed.posts.length == 0) return refreshFeed()
                let respost = feed.posts[0];
                console.log(respost);

                post.authorWall = respost.user_id.wallpaper;
                post.authorId = respost.user_id.id;
                post.authorImg = respost.user_id.avatar;
                post.authorName = respost.user_id.name + " " + respost.user_id.surname;
                post.postType = respost.type;
                let time = new Date(+respost.date * 1000);
                post.timeText = (time.getDate() < 10 ? "0" : "") + time.getDate() + "." + ((1 + time.getMonth()) < 10 ? "0" : "") + (1 + time.getMonth()) + "." + time.getFullYear();
                post.picture = respost.picture;
                post.text = respost.post;
                post.icon = 'images/response.png';
                post.count = respost.responses;
                post.id = respost.id;
                feed.posts.shift()

            } else {
                //photos
                if (feed.photos.length == 0) return refreshFeed()
                let respost = feed.photos[0];
                console.log(respost);
                post.authorWall = respost.user_id.wallpaper;
                post.authorId = respost.user_id.id;
                post.authorImg = respost.user_id.avatar;
                post.authorName = respost.user_id.name + " " + respost.user_id.surname;
                post.postType = 'photo';
                let time = Math.round((Date.now() - +respost.add_time * 1000) / 1000);
                if (time < 60) {
                    post.timeText = Math.round(time) + ' seconds ago';
                } else {
                    time /= 60
                    if (time < 60) {
                        post.timeText = Math.round(time) + ' minute(s) ago';
                    } else {
                        time /= 60
                        if (time < 24) {
                            post.timeText = Math.round(time) + ' hour(s) ago';
                        } else {
                            time /= 24
                            post.timeText = Math.round(time) + ' day(s) ago';
                        }
                    }
                }
                post.picture = respost.photo;
                post.text = respost.description;
                post.icon = 'images/like.png';
                post.count = respost.likes || 0;
                post.id = respost.id;
                feed.photos.shift()

            }
            let postBlock = $(`<div class="flex col post-block">
                <div class="flex post-header">
                    <div class="author-avatar" data-name="${post.authorName}" data-src="${post.authorImg}" data-wall="${post.authorWall}" data-id="${post.authorId}"></div>
                    <div class="author-data">
                        <h2 class="author-name">${post.authorName} posted a(n) <span class="text-border">${post.postType}</span></h2>
                        <h4 class="post-time">${post.timeText}</h4>
                    </div>
                </div>
                <div class="post-picture" data-src="${post.picture}"></div>
                <div class="flex post-footer">
                    <span class="post-text">${post.text}</span>
                    <div class="flex">
                        <img src="${post.icon}" data-type="${post.postType}" data-id="${post.id}" class="btn-img">
                        <span class="post-count" data-id="${post.id}" data-type="${post.postType}">${post.count}</span>
                    </div>
                </div>
                <div class="flex post-comment-input">
                    <input type="text" class="comment-input" placeholder="Comment...">
                    <img src="images/send_blue.png" class="btn-img comment-send" data-type="${post.postType}" data-id="${post.id}">
                </div>
                <div class="post-comments flex col"></div>
            </div>`);
            $('.feed-block').append(postBlock)
        }
    }
    $(document).on('click', '.author-avatar', openUserPage)
    $(document).on('click', '.post-footer .btn-img', function() {
        $(this).off()
        let _this = $(this);
        let type = $(this).attr('data-type');
        let id = $(this).attr('data-id');
        let countText = $('.post-count[data-id=' + id + '][data-type=' + type + ']');
        type = type == 'photo' ? 'like' : 'response';
        $.post({
            url: 'manage.php',
            dataType: 'json',
            data: {
                action: type,
                id,
                user_id: userId
            },
            success: function(res) {
                if (res.ok) {
                    let count = +countText.text();
                    countText.text(++count);
                }
                _this.attr('title', 'You already give your ' + type).css({
                    cursor: 'not-allowed'
                })
            }
        })
    })

    function checkUnread(userArr) {
        $.post({
            url: 'manage.php',
            dataType: 'json',
            data: {
                action: 'unread',
                from: userArr.id,
                to: userId
            },
            success: function(res) {
                if (res.ok == 'true') {
                    $('.messenger li[data-id=' + userArr.id + ']').addClass('unread')
                }
            }
        })
    }

    function msgBadge() {
        $.post({
            url: 'manage.php',
            dataType: 'json',
            data: {
                action: 'unread_badge',
                to: userId
            },
            success: function(res) {
                if (res.ok == 'true') {
                    $('.msg .badge').fadeIn()
                } else {
                    $('.msg .badge').fadeOut()
                }
            }
        })
    }
    setInterval(msgBadge, 1000)

    $('#send-msg').on('click', function() {
        let id = $(this).attr('data-id');
        $('#new-msg').fadeIn()
        $('#send-msg-ok').attr('data-id', id)
    })
    $('#send-msg-ok').on('click', sendMessage)
    $('#withdraw').on('click', withdrawRequest)
    $('#add-friend').on('click', addToFriends)
    $('#stop').on('click', removeFromFriends)
    $('#msg').on('click', function() {
        $.post({
            url: 'manage.php',
            dataType: 'json',
            data: {
                action: 'get_contacts',
                id: userId
            },
            success: function(res) {
                $('.messenger ul').empty();
                for (const contact in res) {
                    let li = $('<li></li>')
                    li.text(res[contact].name).attr({
                        'data-id': res[contact].id,
                        'data-surname': res[contact].surname,
                        'data-avatar': res[contact].avatar,
                        'data-session': res[contact].online
                    }).appendTo('.messenger ul')
                    checkUnread(res[contact])
                }
            }
        })
        $('.messenger').toggle('slide');
        $('.chat-box').hide('slide');
    })
    $(document).on('click', '.messenger li', function() {
        $(this).removeClass('unread').css({
            color: 'rgb(48,52,63)'
        })
        $('.messenger').hide('slide')
        $('.chat-box').show('slide');
        let chater = $(this).text() + ' ' + $(this).attr('data-surname')
        let chater_id = $(this).attr('data-id')
        $('.chat-with').text(chater).attr('data-id', chater_id)
        msgsCount = 0
        loadMsgs(chater_id, 10)
    })
    $('.messages-block').scroll(function() {
        if ($('.messages-block').scrollTop() == 0) {
            loadMsgs($('.chat-with').attr('data-id'), 5)
            $('.messages-block').scrollTop(1)
        }
    })

    $('#exit').click(function() {
        location.replace('exit.php')
    })
    $('.cancel').click(function() {
        $(this).parent().parent().parent().fadeOut();
    })
    $('#edit').click(function() {
        $('#update').fadeIn();
    })
    $('#avatar').on('change', function() {
        document.getElementById('avatar-form').submit();
    })
    $('#wall').on('change', function() {
        document.getElementById('wall-form').submit();
    })
    $('#add-photo-btn').on('change', function() {
        let img = $(this).val()
        $('#photo-comment').fadeIn()
        $('.added-photo').css('background-image', img)
    })
    $('#upl-picture').on('change', function() {
        if (document.getElementById('upl-picture').files.length > 0) {
            $('#picture-label').text('Picked').addClass('orange-btn');
        } else $('#picture-label').text('Pick a picture').removeClass('orange-btn')
    })
    $('#registration').on('click', function() {
        let data = {
            name: $('#reg-form .name').val(),
            surname: $('#reg-form .surname').val(),
            dob: $('#reg-form .dob').val(),
            sex: $('#reg-form .sex').val(),
            email: $('#reg-form .email').val()
        }
        localStorage.setItem('reg', JSON.stringify(data))
        document.getElementById('reg-form').submit();
    })
    $('.change-avatar').mouseover(function() {
        $(this).attr('src', 'images/wall_btn_hover.png')
    }).mouseout(function() {
        $(this).attr('src', 'images/wall_btn.png')
    })
    $('.profile-menu button').on('click', function() {
        let section = $(this).attr('id')
        if (section != 'album' && section != 'friends' && section != 'feed') {
            $('section:not(.' + section + ')').slideUp()
            $('.' + section).slideToggle(500);
        }
    })
    $('#feed').on('click', function() {
        $('section:not(.feed)').slideUp()
        $('.feed').slideToggle(500);
        if (!$('.feed').is(':hidden')) {
            refreshFeed()
        } else {
            $('.loading').remove()
            $('.load-more').remove()
        }
    })
    $('#post-text').on('input', function() {
        let text = $(this).val()
        if (text.length > 140) {
            text.length = 140;
            $(this).val(text);
        }
    })
    $('#album').on('click', function() {
        $('section:not(.album)').slideUp()
        $('.album').slideToggle(500);
        showAlbum($(this).attr('data-id'))
    })

    function showAlbum(id) {
        $.post({
            url: 'manage.php',
            dataType: 'json',
            data: {
                action: 'get_photos',
                id
            },
            success: function(res) {
                let addPhoto = $('.add-photo').clone(true);
                $('.album>.flex').empty()
                for (const img of res) {
                    let a = $('<a class="images" data-lightbox="album"></a>')
                    a.attr({
                        'href': img.photo,
                        'data-title': img.description,
                        'data-id': img.id
                    }).append(`<div data-src="${img.photo}" class="album-photo"></div>`)
                    $('.album>.flex').append(a)
                }
                $('.album>.flex').append(addPhoto)
            }
        })
    }

    function showFriends() {
        let id = $(this).attr('data-id');
        $.post({
            url: 'manage.php',
            dataType: 'json',
            data: {
                action: 'get_friends',
                id
            },
            success: function(res) {
                $('section:not(.friends)').slideUp()
                $('.friends').slideToggle(500);
                $('.friends>.flex').empty()
                for (const friend of res) {
                    let block = $(`<div class="friend-block flex col"></div>`)
                    block.attr({
                        'data-id': friend.id,
                        'data-wall': friend.wallpaper
                    }).on('click', openUserPage)
                    let fAva = $(`<div class="friend-ava" data-src="${friend.avatar}"><div class="badge-status" data-color="${friend.online!='0'?'limegreen':'grey'}"></div></div>`)
                    fAva.appendTo(block)
                    let name = $(`<h3 class="text-border">${friend.name} ${friend.surname}</h3>`)
                    name.appendTo(block)
                    $('.friends>.flex').append(block)
                }
            }
        })
    }
    $('#friends').on('click', showFriends)

})