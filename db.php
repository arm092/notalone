<?php
class Database
{
    private $db;
    public function __construct($base)
    {
        $this->db = new mysqli("localhost", "root", "", $base);
	    if ($this->db->connect_errno) {
		    exit("Database connection error: " . $this->db->connect_error);
	    }
        $this->db->set_charset("utf8");
    }
    public function delete($table, $where, $conect)
    {
        $query = "DELETE FROM $table WHERE";
        $key = array_keys($where);
        $value = array_values($where);
        for ($i = 0; $i < count($key); $i++) {
            $query .= " $key[$i]='$value[$i]' ";
            if ($i < count($key) - 1) {
                $query .= $conect;
            }
        }
        $this->db->query($query);
    }

    public function update($table, $where, $conect, $update)
    {
        $query = "UPDATE $table SET ";
        foreach ($update as $key => $value) {
            $query .= "$key='$value', ";
        }
        $query = substr($query, 0, -2);
        if (!empty($where)) {
            $query .= " WHERE";
            $keys = array_keys($where);
            $values = array_values($where);
            for ($i = 0; $i < count($keys); $i++) {
                $query .= " $keys[$i]='$values[$i]' ";
                if ($i < count($keys) - 1) {
                    $query .= $conect;
                }
            }
        }
        $this->db->query($query);
    }
    public function insert($table, $data)
    {
        $query = "INSERT INTO $table";
        $key = array_keys($data);
        $value = array_values($data);
        $query .= " (" . implode(",", $key) . ") VALUES ('" . implode("','", $value) . "')";
        $this->db->query($query);
    }
    public function select($table, $where, $conect, $selected = "*", $by = "")
    {
        $query = "SELECT $selected FROM $table";
        if (!empty($where)) {
            $query .= " WHERE";
            $key = array_keys($where);
            $value = array_values($where);
            for ($i = 0; $i < count($key); $i++) {
                $query .= " $key[$i]='$value[$i]' ";
                if ($i < count($key) - 1) {
                    $query .= $conect;
                }
            }
            $query .= $by;
        }
        return $this->db->query($query);
    }
    public function select_posts($id, $sex, $dob)
    {
        $age = date('Y') - date('Y', strtotime($dob));
        $today = strtotime(date('d.m.Y'));
        $query = "SELECT * FROM posts WHERE age_start<='$age' AND age_end>='$age' AND (sex='$sex' OR sex='unimportantly') AND date>='$today' AND user_id!='$id' ORDER BY post_time DESC";
        return $this->db->query($query);
    }
    public function select_msgs($user_id, $chater_id, $limit)
    {
        $query = "SELECT
        messages.id,
        messages.from_id,
        messages.message,
        messages.date
        FROM
        messages
        WHERE
        messages.from_id = '$user_id' AND
        messages.to_id = '$chater_id'
        UNION
        SELECT
        messages.id,
        messages.from_id,
        messages.message,
        messages.date
        FROM
        messages
        WHERE
        messages.from_id = '$chater_id' AND
        messages.to_id = '$user_id'
        ORDER BY date DESC LIMIT $limit";
        // $read_msgs = "UPDATE messages
        // SET status = 0
        // WHERE
        // ( messages.from_id = '$user_id' AND messages.to_id = '$chater_id' )
        // OR ( messages.from_id = '$chater_id' AND messages.to_id = '$user_id' )";
        // $this->db->query($read_msgs);
        return $this->db->query($query);
    }
    public function last_id()
    {
        return $this->db->insert_id;
    }
}