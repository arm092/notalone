<?php
session_start();
if (!isset($_SESSION['email'])) {
    header("location:index.php");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include "controller.php";
    switch ($_POST['action']) {
        case 'read':
            $control->read_notif($_POST['id']);
            break;
        // case 'search':
        //     $all_users = $control->get_all_users();
        //     $result = [];
        //     $search = $_POST['value'];
        //     foreach ($all_users as $user) {
        //         if (strpos($user['name'], $search) !== false || strpos($user['surname'], $search) !== false) {
        //             $result[] = $user;
        //         }
        //     }
        //     print json_encode($result);
        //     break;
        case 'is_friend':
            if (!$control->check_request($_POST['id'], $_SESSION['id'])) {
                $res = $control->is_friend($_SESSION['id'], $_POST['id']) || $control->is_friend($_POST['id'], $_SESSION['id']) ? 'true' : 'false';
            } else {
                $res = 'sent';
            }
            print json_encode(array('ok' => $res));
            break;
        case 'f_request':
            $control->send_friend_request($_SESSION['id'], $_POST['id']);
            break;
        case 'f_withdraw':
            $control->withdraw_friend_request($_SESSION['id'], $_POST['id']);
            break;
        case 'f_remove':
            if ($control->is_friend($_SESSION['id'], $_POST['id'])) {
                $control->remove_from_friends($_SESSION['id'], $_POST['id']);
            } elseif ($control->is_friend($_POST['id'], $_SESSION['id'])) {
                $control->remove_from_friends($_POST['id'], $_SESSION['id']);
            }
            break;
        case 'get_user':
            $all_users = $control->get_all_users();
            print json_encode($all_users[$_POST['id'] - 1]);
            break;
        case 'get_notifications':
            $notifs = $control->get_notifications($_POST['id']);
            print json_encode($notifs);
            break;
        case 'get_contacts':
            $msgs = $control->get_user_contacts($_POST['id']);
            print json_encode($msgs);
            break;
        case 'get_msgs':
            $msgs = $control->get_user_msgs($_POST['id'], $_POST['chater_id'], $_POST['limit']);
            print json_encode($msgs);
            break;
        case 'unread':
            $unread = $control->check_unread_msgs($_POST['from'], $_POST['to']);
            print json_encode(['ok' => $unread ? 'true' : 'false']);
            break;
        case 'unread_badge':
            $unread = $control->check_unread_badge($_POST['to']);
            print json_encode(['ok' => $unread ? 'true' : 'false']);
            break;
        case 'get_posts':
            $posts = $control->get_user_posts($_POST['id']);
            print json_encode($posts);
            break;
        case 'get_photos':
            $photos = $control->get_user_photos($_POST['id']);
            print json_encode($photos);
            break;
        case 'get_likes':
            $likes = $control->get_likes_count($_POST['photo']);
            print json_encode(array('count' => $likes));
            break;
        case 'get_responses':
            $resps = $control->get_responses_count($_POST['resp']);
            print json_encode(array('count' => $resps));
            break;
        case 'get_friends':
            $friends = $control->get_user_info($_POST['id'])['friends'];
            print json_encode($friends);
            break;
        case 'get_feed':
            $feed = $control->get_feed($_POST['id']);
            // $users = $control->get_all_users();
            // foreach ($feed['posts'] as $post) {
            //     $post['user_id'] = $users[$post['user_id'] - 1];
            // }
            // foreach ($feed['photos'] as $photo) {
            //     $photo['user_id'] = $users[$photo['user_id'] - 1];
            // }
            print json_encode($feed);
            break;
        case 'set_avatar':
            $new_avatar = $control->get_photo($_POST['new_avatar']);
            $control->update_user_info($_SESSION['email'], ['avatar' => $new_avatar['photo']]);
            break;
        case 'del_photo':
            $control->del_photo($_POST['photo']);
            break;
        case 'accept':
            $control->accept_request($_POST['id']);
            break;
        case 'decline':
            $control->decline_request($_POST['id']);
            break;
        case 'like':
            $like = $control->like($_POST['id'], $_POST['user_id']);
            print json_encode(array('ok' => $like));
            break;
        case 'response':
            $response = $control->response($_POST['id'], $_POST['user_id']);
            print json_encode(array('ok' => $response));
            break;
        case 'msg':
            $control->send_msg($_SESSION['id'], $_POST['to'], $_POST['msg']);
            break;
        default:
            # code...
            break;
    }
} else {
    header("location:index.php");
}