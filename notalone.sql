/*
 Navicat Premium Data Transfer

 Source Server         : db
 Source Server Type    : MySQL
 Source Server Version : 100133
 Source Host           : localhost:3306
 Source Schema         : notalone

 Target Server Type    : MySQL
 Target Server Version : 100133
 File Encoding         : 65001

 Date: 14/09/2018 20:00:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for friends
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `friend_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `friend_id`(`friend_id`) USING BTREE,
  CONSTRAINT `friend_id` FOREIGN KEY (`friend_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of friends
-- ----------------------------
INSERT INTO `friends` VALUES (1, 1, 4);
INSERT INTO `friends` VALUES (5, 2, 1);
INSERT INTO `friends` VALUES (9, 2, 7);
INSERT INTO `friends` VALUES (10, 7, 1);
INSERT INTO `friends` VALUES (11, 3, 4);
INSERT INTO `friends` VALUES (13, 7, 4);
INSERT INTO `friends` VALUES (14, 7, 6);
INSERT INTO `friends` VALUES (16, 7, 3);
INSERT INTO `friends` VALUES (17, 1, 3);
INSERT INTO `friends` VALUES (18, 4, 2);
INSERT INTO `friends` VALUES (19, 8, 2);
INSERT INTO `friends` VALUES (20, 2, 5);
INSERT INTO `friends` VALUES (21, 1, 5);
INSERT INTO `friends` VALUES (22, 4, 5);
INSERT INTO `friends` VALUES (23, 5, 6);
INSERT INTO `friends` VALUES (24, 6, 4);
INSERT INTO `friends` VALUES (25, 6, 2);

-- ----------------------------
-- Table structure for likes
-- ----------------------------
DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo_id` int(11) NULL DEFAULT NULL,
  `liker_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `photo_id`(`photo_id`) USING BTREE,
  INDEX `liker_id`(`liker_id`) USING BTREE,
  CONSTRAINT `liker_id` FOREIGN KEY (`liker_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `photo_id` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of likes
-- ----------------------------
INSERT INTO `likes` VALUES (1, 5, 2);
INSERT INTO `likes` VALUES (2, 5, 3);
INSERT INTO `likes` VALUES (3, 5, 5);
INSERT INTO `likes` VALUES (4, 5, 6);
INSERT INTO `likes` VALUES (5, 5, 4);
INSERT INTO `likes` VALUES (6, 5, 8);
INSERT INTO `likes` VALUES (7, 13, 1);
INSERT INTO `likes` VALUES (8, 14, 1);
INSERT INTO `likes` VALUES (9, 1, 4);
INSERT INTO `likes` VALUES (10, 5, 4);
INSERT INTO `likes` VALUES (11, 1, 4);
INSERT INTO `likes` VALUES (12, 1, 4);
INSERT INTO `likes` VALUES (13, 6, 4);
INSERT INTO `likes` VALUES (14, 15, 1);
INSERT INTO `likes` VALUES (15, 16, 1);
INSERT INTO `likes` VALUES (16, 17, 1);
INSERT INTO `likes` VALUES (17, 12, 1);
INSERT INTO `likes` VALUES (18, 1, 7);
INSERT INTO `likes` VALUES (19, 34, 1);
INSERT INTO `likes` VALUES (20, 12, 7);
INSERT INTO `likes` VALUES (21, 3, 7);
INSERT INTO `likes` VALUES (22, 2, 7);
INSERT INTO `likes` VALUES (23, 4, 7);
INSERT INTO `likes` VALUES (24, 5, 7);
INSERT INTO `likes` VALUES (25, 27, 1);
INSERT INTO `likes` VALUES (26, 18, 1);
INSERT INTO `likes` VALUES (27, 31, 1);
INSERT INTO `likes` VALUES (28, 23, 6);
INSERT INTO `likes` VALUES (29, 20, 6);
INSERT INTO `likes` VALUES (30, 32, 6);
INSERT INTO `likes` VALUES (31, 6, 6);
INSERT INTO `likes` VALUES (32, 1, 1);
INSERT INTO `likes` VALUES (33, 2, 1);
INSERT INTO `likes` VALUES (34, 5, 1);
INSERT INTO `likes` VALUES (35, 20, 7);
INSERT INTO `likes` VALUES (36, 36, 7);
INSERT INTO `likes` VALUES (37, 26, 1);
INSERT INTO `likes` VALUES (38, 35, 1);
INSERT INTO `likes` VALUES (39, 30, 4);
INSERT INTO `likes` VALUES (40, 31, 4);

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `from_id` int(11) NULL DEFAULT NULL,
  `to_id` int(11) NULL DEFAULT NULL,
  `date` int(255) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `to_id_msg`(`to_id`) USING BTREE,
  INDEX `from_id_msg`(`from_id`) USING BTREE,
  CONSTRAINT `from_id_msg` FOREIGN KEY (`from_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `to_id_msg` FOREIGN KEY (`to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES (1, 'Privet aper...... es qani ory anpayman gnanq Abovyan......', 1, 4, 1535875636, 0);
INSERT INTO `messages` VALUES (2, 'Qez mi or shut xabar kanem....', 4, 1, 1535875720, 0);
INSERT INTO `messages` VALUES (3, 'Barev Arpine jan.....', 1, 2, 1535875900, 1);
INSERT INTO `messages` VALUES (4, 'Abooo.... fikusy erb es berum???', 1, 7, 1535875500, 0);
INSERT INTO `messages` VALUES (5, 'Aper, vor prcnes mi hat zangi', 1, 3, 1535875900, 1);
INSERT INTO `messages` VALUES (6, 'Anpayman xabar ara......vortev karevora', 1, 4, 1535875800, 0);
INSERT INTO `messages` VALUES (7, 'erexu atamhatiky erba???', 1, 7, 1535875900, 0);
INSERT INTO `messages` VALUES (8, 'ay axper.... es ur es korel chkas???', 4, 3, 1535875900, 1);
INSERT INTO `messages` VALUES (9, 'amsi 29-in gorci es???', 4, 7, 1535875700, 0);
INSERT INTO `messages` VALUES (10, 'Апер.... дасерт мнацела.....\nсоциальная сеть ов пти сарки???', 1, 4, 1535875936, 0);
INSERT INTO `messages` VALUES (11, 'Privet aper', 4, 1, 1535924736, 0);
INSERT INTO `messages` VALUES (12, 'inch ka??', 4, 1, 1535925295, 0);
INSERT INTO `messages` VALUES (13, 'havai', 1, 4, 1535925756, 0);
INSERT INTO `messages` VALUES (14, 'du asa', 1, 4, 1535926088, 0);
INSERT INTO `messages` VALUES (15, 'im mot el nuyny', 4, 1, 1535926456, 0);
INSERT INTO `messages` VALUES (16, 'ba inch arir?', 4, 1, 1535926815, 0);
INSERT INTO `messages` VALUES (17, 'et soketnery dzir??', 4, 1, 1535926911, 0);
INSERT INTO `messages` VALUES (19, '??', 4, 1, 1535927035, 0);
INSERT INTO `messages` VALUES (20, 'ap?', 4, 1, 1535930524, 0);
INSERT INTO `messages` VALUES (21, 'hn mi ban exav??', 4, 1, 1535947291, 0);
INSERT INTO `messages` VALUES (22, 'voncor....', 1, 4, 1535947926, 0);
INSERT INTO `messages` VALUES (23, 'kisat-prat ashxatuma', 1, 4, 1535950219, 0);
INSERT INTO `messages` VALUES (24, 'hmm', 1, 4, 1535950618, 0);
INSERT INTO `messages` VALUES (26, '2?', 1, 4, 1535950648, 0);
INSERT INTO `messages` VALUES (28, 'hly hima nayi', 1, 4, 1535956457, 0);
INSERT INTO `messages` VALUES (29, 'voncor linuma??', 4, 1, 1535956466, 0);
INSERT INTO `messages` VALUES (30, 'vrodi', 1, 4, 1535956480, 0);
INSERT INTO `messages` VALUES (31, 'exav arden', 4, 1, 1535956541, 0);
INSERT INTO `messages` VALUES (32, 'haaaa', 1, 4, 1535956545, 0);
INSERT INTO `messages` VALUES (33, 'JS-ov piti anei patsoren', 1, 4, 1535956560, 0);
INSERT INTO `messages` VALUES (34, 'du dre php-ov eir uzum??', 4, 1, 1535956574, 0);
INSERT INTO `messages` VALUES (35, 'haaaaa', 1, 4, 1535956578, 0);
INSERT INTO `messages` VALUES (36, 'mnac, vor miangamic kardacvi namaknery.....', 1, 4, 1535956594, 0);
INSERT INTO `messages` VALUES (37, 'u scrol-topi vra handler dnem', 1, 4, 1535956612, 0);
INSERT INTO `messages` VALUES (38, 'vor hin smsnery zagruzka lini??', 4, 1, 1535956623, 0);
INSERT INTO `messages` VALUES (39, 'haaa', 1, 4, 1535956627, 0);
INSERT INTO `messages` VALUES (40, 'Inch ka, chzangir', 1, 3, 1535956643, 1);
INSERT INTO `messages` VALUES (42, 'hima??', 1, 3, 1535956739, 1);
INSERT INTO `messages` VALUES (48, 'erku?', 1, 4, 1535956756, 0);
INSERT INTO `messages` VALUES (62, 'toshnia', 1, 4, 1535957262, 0);

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `from_id` int(11) NULL DEFAULT NULL,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `post_id` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id_notif`(`user_id`) USING BTREE,
  INDEX `from_id`(`from_id`) USING BTREE,
  CONSTRAINT `from_id` FOREIGN KEY (`from_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_id_notif` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of notifications
-- ----------------------------
INSERT INTO `notifications` VALUES (19, 5, 4, 'response', 1, 1);
INSERT INTO `notifications` VALUES (23, 4, 1, 'like', 1, 0);
INSERT INTO `notifications` VALUES (24, 4, 1, 'like', 2, 0);
INSERT INTO `notifications` VALUES (25, 4, 1, 'response', 1, 0);
INSERT INTO `notifications` VALUES (26, 5, 1, 'response', 2, 1);
INSERT INTO `notifications` VALUES (27, 5, 1, 'response', 3, 1);
INSERT INTO `notifications` VALUES (29, 2, 1, 'like', 1, 0);
INSERT INTO `notifications` VALUES (52, 1, 7, 'response', 10, 0);
INSERT INTO `notifications` VALUES (53, 1, 7, 'response', 9, 0);
INSERT INTO `notifications` VALUES (54, 1, 7, 'like', 1, 0);
INSERT INTO `notifications` VALUES (55, 1, 4, 'response', 10, 0);
INSERT INTO `notifications` VALUES (56, 4, 1, 'like', 34, 0);
INSERT INTO `notifications` VALUES (57, 2, 7, 'like', 12, 1);
INSERT INTO `notifications` VALUES (58, 5, 7, 'response', 8, 1);
INSERT INTO `notifications` VALUES (59, 1, 7, 'like', 3, 0);
INSERT INTO `notifications` VALUES (60, 1, 7, 'like', 2, 0);
INSERT INTO `notifications` VALUES (61, 1, 7, 'like', 4, 0);
INSERT INTO `notifications` VALUES (62, 1, 7, 'like', 5, 0);
INSERT INTO `notifications` VALUES (63, 3, 1, 'like', 27, 0);
INSERT INTO `notifications` VALUES (64, 6, 1, 'like', 18, 0);
INSERT INTO `notifications` VALUES (65, 7, 1, 'like', 31, 0);
INSERT INTO `notifications` VALUES (66, 5, 6, 'like', 23, 1);
INSERT INTO `notifications` VALUES (67, 7, 6, 'like', 20, 0);
INSERT INTO `notifications` VALUES (68, 1, 6, 'like', 32, 0);
INSERT INTO `notifications` VALUES (69, 1, 6, 'like', 6, 0);
INSERT INTO `notifications` VALUES (70, 6, 1, 'request', 0, 1);
INSERT INTO `notifications` VALUES (71, 1, 1, 'like', 1, 0);
INSERT INTO `notifications` VALUES (72, 1, 1, 'like', 2, 0);
INSERT INTO `notifications` VALUES (73, 3, 1, 'like', 26, 1);
INSERT INTO `notifications` VALUES (74, 7, 1, 'like', 35, 1);
INSERT INTO `notifications` VALUES (75, 7, 4, 'like', 30, 1);
INSERT INTO `notifications` VALUES (76, 7, 4, 'like', 31, 1);

-- ----------------------------
-- Table structure for photos
-- ----------------------------
DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `add_time` int(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id_photo`(`user_id`) USING BTREE,
  CONSTRAINT `user_id_photo` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of photos
-- ----------------------------
INSERT INTO `photos` VALUES (1, 'images/users_photos/1/5b7c9094480582DE4DB13-F744-430D-A526-D7614301BC29.png', 1, 'Es u serss', 1535000505);
INSERT INTO `photos` VALUES (2, 'images/users_photos/1/5b7c90a501138038e10_410444a1e2104d538b7cfb17da2c1a03.jpg', 1, 'Axpernerov))', 1535100505);
INSERT INTO `photos` VALUES (3, 'images/users_photos/1/5b7c9678744c4038e10_410f8acc399a4364b75263e3c92751ff_mv2.jpg', 1, 'Trashs anush!', 1535110505);
INSERT INTO `photos` VALUES (4, 'images/users_photos/1/5b7c98bb9b440038e10_91da6b0b30c84b32a410d505fc6136da_mv2_d_1653_2339_s_2.jpg', 1, 'Erb nra quny chi tanum))', 1535110505);
INSERT INTO `photos` VALUES (5, 'images/users_photos/1/5b7c99e7bf8e32018-03-23 23-30-33_1521969124163.JPG', 1, 'Selfie))', 1535110505);
INSERT INTO `photos` VALUES (6, 'images/users_photos/1/5b7c9a4f1136a2018-03-23 23-30-34_1521969058014.JPG', 1, 'Nature...', 1535110505);
INSERT INTO `photos` VALUES (7, 'images/users_photos/1/5b7c9a9a0f40812745916_587928654688289_6080889374361362210_n.jpg', 1, 'Me...', 1535110505);
INSERT INTO `photos` VALUES (8, 'images/users_photos/1/5b7cc0aa3aa6c10532383_493086140839208_8015866080332550554_n.jpg', 1, 'Tatev Vank...', 1535110505);
INSERT INTO `photos` VALUES (10, 'images/users_photos/1/5b7d5fc859d202018-03-24 09-57-22.JPG', 1, 'At work...', 1535110505);
INSERT INTO `photos` VALUES (12, 'images/users_photos/2/5b7f4bc376d23Arpine.jpg', 2, 'Me', 1535110505);
INSERT INTO `photos` VALUES (13, 'images/users_photos/4/5b7fd5f3b34462014-02-16 03-24-43.JPG', 4, 'Manukyani dasin....', 1535110505);
INSERT INTO `photos` VALUES (14, 'images/users_photos/4/5b7fd608621342015-06-25 23-33-05.JPG', 4, 'Nuyn maykeqov)))', 1535110505);
INSERT INTO `photos` VALUES (15, 'images/users_photos/4/5b7fd623277222016-04-27 21-06-31.JPG', 4, 'Kacheli....', 1535110505);
INSERT INTO `photos` VALUES (16, 'images/users_photos/4/5b7fd6370ffed2016-05-01 17-59-17.JPG', 4, 'Waiting....', 1535110505);
INSERT INTO `photos` VALUES (17, 'images/users_photos/4/5b7fd65d813e42018-03-23 23-30-26_1521969502681.JPG', 4, 'Parz lake', 1535110505);
INSERT INTO `photos` VALUES (18, 'images/users_photos/6/5b7fd86b78037profile_pic_of_Արսեն_Մանուկյան_file1521121331.jpeg', 6, 'me.....', 1535110505);
INSERT INTO `photos` VALUES (19, 'images/users_photos/7/5b810e790ef822018-03-23 23-30-57_1521968398458.JPG', 7, 'Sups...', 1535184505);
INSERT INTO `photos` VALUES (20, 'images/users_photos/7/5b810e8901e2a01e983b3f4262452b70cc5f71e1d1f7e.jpg', 7, 'Black panther', 1535184521);
INSERT INTO `photos` VALUES (21, 'images/users_photos/7/5b810e988c0e32018-03-23 23-30-55_1521968630728.JPG', 7, 'Cap)', 1535184536);
INSERT INTO `photos` VALUES (23, 'images/users_photos/5/5b8112427888eprofile_pic_of_Ռաֆայել_Գրիգորյան_file1526984722.jpeg', 5, 'Mee..........', 1535185474);
INSERT INTO `photos` VALUES (25, 'images/users_photos/3/5b8252015eaed2018-03-23 23-30-28.JPG', 3, 'parz lake....', 1535267329);
INSERT INTO `photos` VALUES (26, 'images/users_photos/3/5b82521d29aaf2018-03-23 23-30-29_1521969450394.JPG', 3, 'my birthday', 1535267357);
INSERT INTO `photos` VALUES (27, 'images/users_photos/3/5b825248628002018-03-23 23-30-29_1521969481689.JPG', 3, 'axperutyuuuns))', 1535267400);
INSERT INTO `photos` VALUES (28, 'images/users_photos/3/5b82527e3f4262018-03-23 23-30-29.JPEG', 3, 'Prcaaaanqqq verjapes))', 1535267454);
INSERT INTO `photos` VALUES (29, 'images/users_photos/3/5b8252ca344f82018-03-23 23-30-35_1521969004725.JPEG', 3, 'Xashi amenamya araroxutyun)', 1535267530);
INSERT INTO `photos` VALUES (30, 'images/users_photos/7/5b8260ae473582018-03-23 23-31-13.JPEG', 7, 'tutaks....', 1535271086);
INSERT INTO `photos` VALUES (31, 'images/users_photos/7/5b8260eaf41502018-03-23 23-30-55_1521968628024.JPG', 7, 'deadpool', 1535271147);
INSERT INTO `photos` VALUES (32, 'images/users_photos/1/5b82663f9b7042018-03-23 23-30-35_1521969006116.JPEG', 1, 'jahel vaxter))', 1535272511);
INSERT INTO `photos` VALUES (33, 'images/users_photos/1/5b863a145765d2012-10-01 14-46-36.JPG', 1, 'Arevangum))))', 1535523348);
INSERT INTO `photos` VALUES (34, 'images/users_photos/4/5b874428024f32016-08-05 15-59-42.JPG', 4, 'Es ur einq gnum????', 1535591464);
INSERT INTO `photos` VALUES (35, 'images/users_photos/7/5b88e73c2b3f312004974_1650927321857470_6074701464664122963_n.jpg', 7, 'uxexov mtaceq))', 1535698748);
INSERT INTO `photos` VALUES (36, 'images/users_photos/7/5b88e74a45bbc12249734_1668792743404261_1431026428465844155_n.jpg', 7, 'arevot....', 1535698762);
INSERT INTO `photos` VALUES (37, 'images/users_photos/7/5b88e75ddaf8920258011_1972839349666264_222072253516004972_n.jpg', 7, 'Varedevor)', 1535698781);

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age_start` int(11) NULL DEFAULT NULL,
  `age_end` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `date` int(20) NULL DEFAULT NULL,
  `post_time` int(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id_post`(`user_id`) USING BTREE,
  CONSTRAINT `user_id_post` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (1, 'Erexeeq, ov kga hetss tatron...\r\nNenc laaav nerkayacuma....\r\n\r\nGhaplanyani anv. dramatikakan tatron\r\n\"Verjin tsaghratsun\"....', 'images/posts/2/5b7fd16638836big_verjicahracu.jpg', 'Invitation', 'unimportantly', 18, 28, 2, 1536962400, 1535103334);
INSERT INTO `posts` VALUES (2, 'Կ. Դեմիրճյանի անվան մարզահամերգային համալիրի համերգային դահլիճում ելույթ կունենան աշխարհահռչակ Comedy Club շոուի աստղերը՝ Գարիկ Մարտիրոսյանը, Գարիկ Խա', 'images/posts/4/5b7fd6f56be62thumbnail.png', 'Offer', 'male', 24, 29, 4, 1537048800, 1535104757);
INSERT INTO `posts` VALUES (3, 'Հովհաննես Դավթյան\r\nՆոր սթենդափ կատակերգություն\r\n\r\nOva gali.....???', 'images/posts/6/5b7fd82dd2275thumbnail (1).png', 'Offer', 'unimportantly', 18, 26, 6, 1534197600, 1535105069);
INSERT INTO `posts` VALUES (4, 'Ova galis? komedya nerkayacuma..... erku hat toms unem', 'images/posts/7/5b810f2aa4747thumbnail (2).png', 'Invitation', 'unimportantly', 20, 30, 7, 1538863200, 1535184682);
INSERT INTO `posts` VALUES (5, 'Օգոստոսի 31-ին, ժամը 19:00-ին «Նարեկացի» արվեստի միությունում տեղի կունենա ութամյա ֆլեյտահարուհի Էլեն Վիրաբյանի՝ «Ֆլեյտայի կախարդական ձայները» խորագիր', 'images/posts/default.png', 'Offer', 'unimportantly', 10, 25, 7, 1535666400, 1535184814);
INSERT INTO `posts` VALUES (6, '«Հայջի» բենդի առաջին ալբոմի շնորհանդեսը` «Մի կտոր արև» բարեգործական համերգի շրջանակներում:', 'images/posts/7/5b81108e19245thumbnail (3).png', 'Offer', 'unimportantly', 16, 30, 7, 1537135200, 1535185038);
INSERT INTO `posts` VALUES (7, 'В Лос-Анджелесе недалекого будущего каждый уважающий себя преступник отправляется за медицинской помощью к суровой Медсестре в отель «Артемида».', 'images/posts/5/5b81116dbb6daslug-87897.jpg', 'Offer', 'male', 19, 28, 5, 1535580000, 1535185261);
INSERT INTO `posts` VALUES (8, 'GoodBye Summer Party with DJ MAY\r\n\r\nova galis??\r\n\r\nbayc mutqy chgitem inchqana))))', 'images/posts/5/5b8113cac121dslug-91045.jpg', 'Offer', 'unimportantly', 18, 29, 5, 1535666400, 1535185866);
INSERT INTO `posts` VALUES (9, 'Мег: Монстр глубины\r\n\r\nUjex kinoya...... Stetxemna xaxum.....\r\nOv kga?', 'images/posts/default.png', 'Invitation', 'male', 22, 28, 1, 1535493600, 1535245204);
INSERT INTO `posts` VALUES (10, 'Ova galis Disneyi comicconin (D23)?\r\nKonventi tomsy mi orva 67$....\r\nSamalyotiny 820$\r\n\r\nAnaheim Convention Center, CA', 'images/posts/1/5b863d4c2e874Expo19_1180x600_General-780x440.jpg', 'Offer', 'unimportantly', 22, 30, 1, 1566511200, 1535524172);
INSERT INTO `posts` VALUES (11, 'Как женить холостяка\r\nlav komedyaya.....\r\nov hets kga?', 'images/posts/default.jpg', 'Invitation', 'male', 20, 30, 7, 1536098400, 1535699298);
INSERT INTO `posts` VALUES (12, 'Lucas Madoyan Blues Band at Mezzo!\r\n\r\nkayfot hamerga....\r\ngalacox ka??', 'images/posts/7/5b88e9db8ac1cslug-87993.jpg', 'Offer', 'unimportantly', 20, 30, 7, 1537135200, 1535699419);
INSERT INTO `posts` VALUES (13, 'Шоу Мистико\r\n\r\nasum en lav kinoya\r\ngalis eq?', 'images/posts/7/5b88eadf6dd8aslug-90963.jpg', 'Offer', 'unimportantly', 18, 30, 7, 1536271200, 1535699679);
INSERT INTO `posts` VALUES (14, 'Ես ապրում եմ Երևանում\r\nԱշոտ Ղազարյան\r\nՀումորային -երաժշտական մոնո ներկայացում:\r\nՆվիրվում է Երևանի 2800-ամյակին:\r\nov kga??', 'images/posts/7/5b88ebdfbd37eslug-88287.jpg', 'Offer', 'unimportantly', 20, 30, 7, 1537394400, 1535699935);
INSERT INTO `posts` VALUES (15, 'Ամեն ինչ լաՅվ ա լինելու կամ Հեղափոխության 55 երանգները\r\nՀայկ Պետրոսյան\r\nEsi lav bocer uni......\r\nkarelia gnal.... ov kga???', 'images/posts/7/5b88ec5c42486slug-91677.jpg', 'Offer', 'unimportantly', 20, 30, 7, 1537740000, 1535700060);
INSERT INTO `posts` VALUES (16, 'Nino Katamadze & Insight in Armenia\r\n\r\nbomb hamerga.....\r\nov kga???', 'images/posts/7/5b88edbe3147dslug-84171.jpg', 'Offer', 'unimportantly', 20, 30, 7, 1538172000, 1535700414);
INSERT INTO `posts` VALUES (17, 'Նոր ներկայացում\r\nՊ.Օ.Կ. դը Բոմարշե\r\n\"Ֆիգարոյի ամուսնությունը\"\r\nՀ. Ղափլանյանի անվան դրամատիկական թատրոն\r\nvoncor lavna.... ov kga?', 'images/posts/7/5b88ee815f093slug-77617.jpg', 'Offer', 'unimportantly', 20, 30, 7, 1537653600, 1535700609);
INSERT INTO `posts` VALUES (18, 'Ինչ տարբերություն, թե ում հետ\r\nԵրևանի Պետական Կամերային Թատրոն\r\nha uzum em gnam, chi stacvum.....\r\nov kga hets???', 'images/posts/7/5b88efa9690c3slug-233.jpg', 'Invitation', 'unimportantly', 20, 30, 7, 1537048800, 1535700905);
INSERT INTO `posts` VALUES (19, 'Չկա տղամարդ՝ չկա պրոբլեմ\r\nԵրևանի Պետական Կամերային Թատրոն\r\nkarelia gnal.....', 'images/posts/7/5b88efef0c051slug-80031.jpg', 'Offer', 'unimportantly', 20, 30, 7, 1537221600, 1535700975);

-- ----------------------------
-- Table structure for responses
-- ----------------------------
DROP TABLE IF EXISTS `responses`;
CREATE TABLE `responses`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `responser` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE,
  INDEX `responser`(`responser`) USING BTREE,
  CONSTRAINT `post_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `responser` FOREIGN KEY (`responser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of responses
-- ----------------------------
INSERT INTO `responses` VALUES (1, 2, 1);
INSERT INTO `responses` VALUES (2, 8, 1);
INSERT INTO `responses` VALUES (3, 7, 1);
INSERT INTO `responses` VALUES (4, 6, 1);
INSERT INTO `responses` VALUES (5, 4, 1);
INSERT INTO `responses` VALUES (7, 10, 7);
INSERT INTO `responses` VALUES (8, 9, 7);
INSERT INTO `responses` VALUES (9, 10, 4);
INSERT INTO `responses` VALUES (10, 8, 7);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `date_of_birth` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wallpaper` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'images/wall.png',
  `marital_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `work_place` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `work_as` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `online` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Arman', 'Khachatryan', 'male', 'armnet-10@mail.ru', '1992-05-10', '$2y$10$0NETVRuv4wxhKZth.U4MR.5KhDhvt87uklTyOq5eEK7/EJa/T3l.K', 'images/users_photos/1/5b7c99e7bf8e32018-03-23 23-30-33_1521969124163.JPG', 'images/users_photos/1/5b8267c3292ea2012_lamborghini_urus_concept_5_1600x1200.jpg', 'Married', 'GoodWin Bet', 'Manager', 'Armenia', 'Yerevan', 'funs5fjte222o8ic21o107odi5');
INSERT INTO `users` VALUES (2, 'Arpine', 'Poghosyan', 'female', 'poghosyanarpine521@gmail.com', '1998-05-21', '$2y$10$C196kAvfPBywyHLoeuRXUerNC4fqdZRhhOdoqJtsZ1v9mA7SvkjC.', 'images/users_photos/2/5b7f4bc376d23Arpine.jpg', 'images/users_photos/2/5b7f4b868a8f2squirrel_winter_snow_color_tail_furry_52971_1680x1050.jpg', 'Single', 'ProfIt De.Co.', 'Trainer', 'Armenia', 'Yerevan', '0');
INSERT INTO `users` VALUES (3, 'Edgar', 'Grigoryan', 'male', 'edgar_17@mail.ru', '1991-07-02', '$2y$10$bbGre8hkKtN.GAwlj62FPe7NGDXk/dPcBj3.yihopwKdKe3Dip1p.', 'images/users_photos/3/5b8251954af4e2018-03-23 23-30-28_1521969488827.JPG', 'images/users_photos/3/5b8251dfb3978assassins_creed_2016_movie_4k_8k-2560x1600.jpg', 'Single', 'Ararat Bank', 'Collector', 'Armenia', 'Yerevan', '0');
INSERT INTO `users` VALUES (4, 'Artur', 'Manukyan', 'male', 'art17_91@mail.ru', '1991-04-18', '$2y$10$63aldQbD/bbbThBISqielOPDU9WtIeJ4yo6ZrCBSpjaDQiHlvaSG2', 'images/users_photos/4/5b7fd58b13d7c2015-02-08 00-52-38.JPG', 'images/users_photos/4/5b811544c92e82018-03-23 23-30-08_1521969796412.JPG', 'Single', 'Beeline', 'Operator', 'Armenia', 'Yerevan', '0');
INSERT INTO `users` VALUES (5, 'Rafael', 'Grigoryan', 'male', 'rafo@bk.ru', '1995-05-19', '$2y$10$MRnXzKkBe0iSuOQiRtiE/OwBkccHKcWvZR.w/jVosEHoPGxBAXyYO', 'images/users_photos/5/5b8112427888eprofile_pic_of_Ռաֆայել_Գրիգորյան_file1526984722.jpeg', 'images/users_photos/5/5b8113eac2f912014-09-24 01-28-21.JPEG', 'Single', 'MotoShop', 'Cashier', 'Armenia', 'Yerevan', '0');
INSERT INTO `users` VALUES (6, 'Arsen', 'Manukyan', 'male', 'ars@bk.ru', '1996-02-28', '$2y$10$IFVye5/vciExEhw151iY/eGaP6sOeYUS.nBUZQi9t9Qtov39alci6', 'images/users_photos/6/5b7fd74bca4e8profile_pic_of_Արսեն_Մանուկյան_file1521121331.jpeg', 'images/users_photos/6/5b7fd7b521d94Sneakers.jpg', 'Single', 'SAS Group', 'Seller', 'Armenia', 'Yerevan', '0');
INSERT INTO `users` VALUES (7, 'Albert', 'Hovhannisyan', 'male', 'abo_bo@mail.ru', '1991-09-13', '$2y$10$Q1i4ajHzrA8Xy/5EwIFts.7cDJs8sWRQIoGblum7eIhY3ga6aB5D6', 'images/users_photos/7/5b88e74a45bbc12249734_1668792743404261_1431026428465844155_n.jpg', 'images/users_photos/7/5b8262a052e3ewp2068827.jpg', 'Married', 'InecoBank', 'Cashier', 'Armenia', 'Artashat', '0');
INSERT INTO `users` VALUES (8, 'Armen', 'Marjinyan', 'male', 'marjinyan@bk.ru', '1988-03-28', '$2y$10$/Iq6tVWBRdrZ4ZLNzgXPK.fD5uIVKixrf8JmoWejroA2N4qgdXbxO', 'images/default_man.png', 'images/wall.png', 'Single', 'ProfIt De.Co.', 'Director', 'Armenia', 'Yerevan', '0');

SET FOREIGN_KEY_CHECKS = 1;
