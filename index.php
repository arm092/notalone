<!DOCTYPE html>
<html lang="en">
<?php
$ses_id = session_id();
if (empty($ses_id)) {
	session_start();
	$ses_id = session_id();
}
if (isset($_SESSION['id'])) {
	header("location:profile.php");
}
?>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="shortcut icon" href="images/logo.png"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>NotAlone</title>
</head>

<body>
<div class="menu flex full">
    <a href="index.php" class="flex">
        <img src="images/logo_brand.png" id="logo">
        <div>
            <h1>NotAlone</h1>
            <h3>Find a company</h3>
        </div>
    </a>
    <form class="flex login" action="login.php" method="POST">
        <input type="email" name="email" placeholder="Your email">
        <input type="password" name="pass" placeholder="Your password">
        <button>Log In</button>
    </form>
</div>
<form action="reg.php" method="POST" class="flex col full" id="reg-form" style="padding-top: 7vw">
    <h2>Create new account...</h2>
    <div class="flex">
        <input name="name" type="text" class="name orange-btn" placeholder="Your name" required>
        <input name="surname" type="text" class="surname orange-btn" placeholder="Your surname" required>
        <input name="dob" type="date" class="dob orange-btn" placeholder="Your date of birth" required>
        <select class="sex orange-btn" name="sex">
            <option value="" selected disabled>Your sex</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
        </select>
    </div>
    <div class="flex">
        <input name="email" type="email" class="email orange-btn" placeholder="Your email" required>
        <input name="pass" type="password" class="pass orange-btn" placeholder="Your password" required>
        <input name="confirm" type="password" class="confirm orange-btn" placeholder="Confirm password" required>
        <button id="registration" class="orange-btn">Registration</button>
    </div>
</form>
<?php
if (isset($_SESSION['error'])) {
	?>
    <div class="error-box">
	    <?= $_SESSION['error'] ?>
    </div>
	<?php
	unset($_SESSION['error']);
}
?>
<script src="js/script.js">
</script>
</body>

</html>